import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArticlesModule} from './articles/articles.module';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {BlogModule} from './blog/blog.module';
import {ThemesModule} from './themes/themes.module';
import {AccountSettingsComponent} from './account-settings/account-settings.component';

import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UserArticlesModule} from './user-articles/user-articles.module';
import {TrainingModule} from './training/training.module';
import {UserThemesModule} from './user-themes/user-themes.module';
import {UserBlogModule} from './user-blog/user-blog.module';
import {UserServicesModule} from './user-services/user-services.module';
import {ServicesModule} from './services/services.module';
import {AuthGaurdNotUserService, AuthGaurdService} from './shared/auth-gaurd.service';
import {UserPurchasesModule} from './user-purchases/user-purchases.module';
import {UserCoursesModule} from './user-courses/user-courses.module';
import {FooterDetailsComponent} from './footer-details/footer-details.component';
import {ResolveService} from './shared/resolve.service';
import {NotificationsComponent} from './notifications/notifications.component';
import {UserMessagesModule} from './user-messages/user-messages.module';
import {StatisticsComponent} from './user-statistics/statistics.component';
import {ResetPasswordModule} from './reset-password/reset-password.module';
import {UserTicketsModule} from './user-tickets/user-tickets.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent

  },
  {
    path: 'register/:affiliate_code',
    component: RegisterComponent,
    canActivate: [AuthGaurdNotUserService]

  },
  {
    path: 'user/reset',
    loadChildren: () => ResetPasswordModule,
    canActivate: [AuthGaurdNotUserService]

  },
  {
    path: 'user/tickets',
    loadChildren: () => UserTicketsModule,
    canActivate: [AuthGaurdService]

  },
  {
    path: 'services',
    loadChildren: () => ServicesModule
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGaurdNotUserService]
  },
  {
    path: 'user/settings',
    component: AccountSettingsComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/messages',
    loadChildren: () => UserMessagesModule,
    canActivate: [AuthGaurdService],
    canLoad: [AuthGaurdService]

  },
  {
    path: 'user/notifications',
    component: NotificationsComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/purchases',
    loadChildren: () => UserPurchasesModule,
    canActivate: [AuthGaurdService],
    canLoad: [AuthGaurdService]
  },
  {
    path: 'user/courses',
    loadChildren: () => UserCoursesModule,
    canActivate: [AuthGaurdService],
    canLoad: [AuthGaurdService]
  },
  {
    path: 'user/services',
    loadChildren: () => UserServicesModule,
    canLoad: [AuthGaurdService],
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/themes',
    loadChildren: () => UserThemesModule,
    canLoad: [AuthGaurdService],
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/blog',
    loadChildren: () => UserBlogModule,
    canLoad: [AuthGaurdService],
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/articles',
    loadChildren: () => UserArticlesModule,
    canLoad: [AuthGaurdService],
    canActivate: [AuthGaurdService]
  },
  {
    path: 'user/statistics',
    component: StatisticsComponent,
    canActivate: [AuthGaurdService]
  },
  {
    path: 'themes',
    loadChildren: () => ThemesModule
    /* loadChildren: 'app/free-themes/free-themes.module#FreeThemesModule' */
  },
  {
    path: 'articles',
    loadChildren: () => ArticlesModule
    /* loadChildren: 'app/free-themes/free-themes.module#FreeThemesModule' */
  },
  {
    path: 'blog',
    loadChildren: () => BlogModule
    /* loadChildren: 'app/free-themes/free-themes.module#FreeThemesModule' */
  },
  {
    path: 'learning',
    loadChildren: () => TrainingModule
    /* loadChildren: 'app/free-themes/free-themes.module#FreeThemesModule' */
  },
  {
    path: 'footer/:name',
    component: FooterDetailsComponent,
    resolve: {
      footer: ResolveService
    }
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingRoutingModule { }
