import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
  providers: [AuthService]

})
export class LoginComponent implements OnInit {
  pageTitle = 'تسجيل الدخول';
  signForm: FormGroup;
  successLogin = undefined;
  errorData: string | null;
  constructor(private fb: FormBuilder, private title: Title, private auth: AuthService, private route: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.signForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['',  [Validators.required]]
    });
  }
  signSubmit(): void {

    if (this.signForm.status === 'VALID') {
      this.auth.login(this.signForm.value).subscribe(value => {
        if (value.status === 'true') {
          this.errorData = null;
          this.successLogin = true;
          localStorage.setItem('token', value.data.token);
          localStorage.setItem('id', value.data.user.id);
          localStorage.setItem('name', value.data.user.name);
          localStorage.setItem('email', value.data.user.email);
          localStorage.setItem('phone', value.data.user.phone);
          localStorage.setItem('affiliate_code', value.data.user.affiliate_code);
          setTimeout(() => {
            location.assign('/');
          }, 500);

        } else {
          this.errorData = 'الايميل او كلمه المرور غير صحيحه';
          this.successLogin = false;
        }
      }, error => {this.errorData = error; this.successLogin = false; });
    } else {
      this.errorData =  'البيانات المدخله غير صحيحه';
      this.successLogin = false;
    }

  }
}
