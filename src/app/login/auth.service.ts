import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Users} from '../shared/users';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private apiUrl = 'http://dashboard.belmagan.com/api/auth/login';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  login(data: Object): Observable<any> {
    return this.Http.post<Users>(this.apiUrl, data).pipe(retry(3), catchError(this.handle.handleError));
  }

}
