import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Themes} from '../shared/Themes';
import { IDownload} from '../shared/Themes';
@Injectable()
export class AuthService {
   private apiFreeUrl = 'http://dashboard.belmagan.com/api/theme/all/free';
   private apiPaidUrl = 'http://dashboard.belmagan.com/api/theme/all/paid';
   private catURL = 'http://dashboard.belmagan.com/api/theme-categories';
  private buyThemeURL = 'http://dashboard.belmagan.com/api/paypal/pay/theme';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getAllFreeThemes(): Observable<any> {
    return this.Http.get<Themes>(this.apiFreeUrl).pipe(retry(3), catchError(this.handle.handleError));
  }
  getAllPaidThemes(): Observable<any> {
    return this.Http.get<Themes>(this.apiPaidUrl).pipe(retry(3), catchError(this.handle.handleError));
  }
  getThemeDetails(id: number | string, name: string, affiliate_code?: number | string): Observable<any> {
    if (affiliate_code !== 'details' && localStorage.getItem('affiliate_code')) {
      return this.Http.get<any>(`http://dashboard.belmagan.com/api/theme/get/${+id}/${name}/${affiliate_code}`).pipe(retry(3), catchError(this.handle.handleError));
    }
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/theme/get/${+id}/${name}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getThemeDownlodURL(id: number | string): Observable<any> {
    return this.Http.get<IDownload>(`http://dashboard.belmagan.com/api/theme/url/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getThemesCat(): Observable<any> {
    return this.Http.get<Themes>(this.catURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getThemesCatData(id: number | string): Observable<any> {
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/theme-categories-items/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getUserNonActiveTheme(id: number | string): Observable<any> {
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/get/theme/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  buyTheme(item: number) {
    return this.Http.post(this.buyThemeURL, {itemId: item}, {responseType: 'text'}).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPage(num: number) {
    return this.Http.get(`http://dashboard.belmagan.com/api/theme/all/free?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPagePaid(num: number) {
    return this.Http.get(`http://dashboard.belmagan.com/api/theme/all/paid?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPageCategory(id: number | string , num: number) {
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/theme-categories-items/${+id}?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));
  }


}
