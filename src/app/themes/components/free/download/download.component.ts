import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AdsDownloadService} from './ads.service';


@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.sass'],
  providers: [AdsDownloadService]
})
export class DownloadFreeComponent implements OnInit {
  download_mirror = true;
  download_mirror1 = false;
  download_mirror2 = false;
  download_mirror3 = false;
  pageTitle = 'تحميل قالب ';
  downloadID: string;
  themes1: any;
  themes2: any;
  themes3: any;
  courses: any;
  articles: any;
  services: any;
  loaderFinished = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';
  affiliate_codeService = localStorage.getItem('affiliate_code') || 'service';
  affiliate_codeArticle = localStorage.getItem('affiliate_code') || 'article';
  constructor(private title: Title, private router: Router, private auth: AuthService, private route: ActivatedRoute, private ads: AdsDownloadService) {
    this.route.paramMap.subscribe(param => this.downloadID = param.get('id'));
    this.ads.getLatestThemes().subscribe(value => this.themes1 = value.data.themes);
    this.ads.getDownloadThemes().subscribe(value => this.themes2 = value.data.themes);
    this.ads.getViewedThemes().subscribe(value => this.themes3 = value.data.themes);
    this.ads.getArticles().subscribe(value => this.articles = value.data);
    this.ads.getCourses().subscribe(value => this.courses = value.data);
    this.ads.getServices().subscribe(value => this.services = value.data, () => 0, () => this.loaderFinished = true);
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

  }
  downloadFile() {
    this.auth.getThemeDownlodURL(this.downloadID).subscribe(value => location.href = value.data);
  }
  slugifyThemes(id: number, type: string, title: string) {
    this.router.navigate([`themes/${type}/${id}/${title}/${this.affiliate_code}`]);
  }

  slugOthers(section: string, id: number, title: string, ) {
    this.router.navigate([`${section}/${+id}/${title}/${this.affiliate_codeArticle}`]);
  }


}
