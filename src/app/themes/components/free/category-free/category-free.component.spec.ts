import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryFreeComponent } from './category-free.component';

describe('CategoryFreeComponent', () => {
  let component: CategoryFreeComponent;
  let fixture: ComponentFixture<CategoryFreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryFreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
