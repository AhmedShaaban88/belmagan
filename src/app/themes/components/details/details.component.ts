import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
import {Title} from '@angular/platform-browser';
import {Themes} from '../../../shared/Themes';
import {AdsService} from '../../../shared/ads.service';

@Component({
  selector: 'app-home',
  templateUrl: './details.component.html',
  styleUrls: ['./detailscomponent.sass'],
})
export class DetailsViewComponent implements OnInit, AfterViewInit {
  theme: Themes;
  failedMsg: string;
  loaderFinished = false;
  advs: any;

  constructor(private router: Router, private title: Title, private route: ActivatedRoute, private ads: AdsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.theme !== undefined && data.theme.data !== 'غير موجوده') {
        this.title.setTitle(` بالمجان - ${data.theme.data.title}`);
        this.theme = data.theme.data;
        this.loaderFinished = true;

      } else {
        this.router.navigate(['not-found']);
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
    this.ads.getAds().subscribe(value => this.advs = value['data']);


  }
  ngAfterViewInit() {
    $('.pre-img').click(function () {
      $('#cover').attr('src', $(this).attr('src'));
      $('.pre-img').removeClass('active');
      $(this).addClass('active');
    });
  }
  openUrl(url: string) {
    location.href = url;
  }
  openAd(url: string) {
    location.assign(url);
  }


}
