import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../../auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.sass'],
})
export class ThemesPaidComponent implements OnInit {
  pageTitle = 'القوالب المدفوعه';
  categories: any;
  allCount: string | number;
  constructor(private router: Router, private title: Title, private auth: AuthService) { }

  ngOnInit() {

    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getThemesCat().subscribe(cat => {this.categories = cat.data; this.allCount = cat.AllCount; });

  }

}
