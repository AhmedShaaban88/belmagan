import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidCategoryComponent } from './paid-category.component';

describe('PaidCategoryComponent', () => {
  let component: PaidCategoryComponent;
  let fixture: ComponentFixture<PaidCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
