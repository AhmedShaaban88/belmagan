import {Component, HostListener, OnInit} from '@angular/core';
import {Themes} from '../../../../shared/Themes';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'app-paid-category',
  templateUrl: './paid-category.component.html',
  styleUrls: ['./paid-category.component.sass']
})
export class PaidCategoryComponent implements OnInit {
  failedMsg: string;
  loaderFinished = false;
  themes: Themes;
  currentPage = 1;
  pageThemes: Themes[] = [];
  pageLoader = false;
  categoryID: number;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';

  constructor(private route: ActivatedRoute, private router: Router, private title: Title, private auth: AuthService) { }

  ngOnInit() {
    this.route.data.subscribe((data) => {
      if (data.theme.data !== undefined) {
        this.pageLoader = false;
        this.themes = data.theme.data.PaidThemes.themes;
        this.categoryID = data.theme.Category.id;
        this.loaderFinished = true;
      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`themes/paid/${+id}/${title}/${this.affiliate_code}`]);
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPageCategory(this.categoryID , ++this.currentPage).subscribe((value: any) => {
        if (value.data.PaidThemes.themes.length > 0) {
          this.pageThemes.push(value.data.PaidThemes.themes);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }


}
