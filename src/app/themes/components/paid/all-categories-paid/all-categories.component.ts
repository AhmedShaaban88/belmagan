import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Themes} from '../../../../shared/Themes';
import {AuthService} from '../../../auth.service';
@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.sass']
})
export class AllCategoriesPaidComponent implements OnInit {
  failedMsg: string;
  loaderFinished = false;
  themes: Themes;
  currentPage = 1;
  pageThemes: Themes[] = [];
  pageLoader = false;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';

  constructor(private route: ActivatedRoute, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
     this.auth.getAllPaidThemes().subscribe(categories => this.themes = categories.data.themes, error => {this.failedMsg = error; this.loaderFinished = true; } , () => this.loaderFinished = true);

  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`themes/paid/${+id}/${title}/${this.affiliate_code}`]);
  }
  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPagePaid(++this.currentPage).subscribe((value: any) => {
        if (value.data.themes.length > 0) {
          this.pageThemes.push(value.data.themes);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }
}
