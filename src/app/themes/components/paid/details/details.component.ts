import {Component, OnInit, AfterViewInit} from '@angular/core';
import * as $ from 'jquery';
import { Title } from '@angular/platform-browser';
import {Themes} from '../../../../shared/Themes';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentsService} from '../../../../shared/comment_rate.service';
import {AdsService} from '../../../../shared/ads.service';
import {AuthService} from '../../../auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './details.component.html',
  styleUrls: ['./detailscomponent.sass']
})
export class DetailsPaidComponent implements OnInit, AfterViewInit {
  userThemes: Themes;
  relatedThemes: Themes;
  theme: Themes;
  failedMsg: string;
  loaderFinished = false;
  newForm: FormGroup;
  name = localStorage.getItem('name');
  email = localStorage.getItem('email');
  commentStatus = undefined;
  userRate: any;
  makeThemeRate = false;
  makeUserRate = false;
  theSameUser = false;
  advs: any;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';
  constructor(private router: Router, private title: Title, private route: ActivatedRoute,
              private comment: CommentsService, private fb: FormBuilder, private ads: AdsService, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.theme !== undefined) {
        this.title.setTitle(` بالمجان - ${data.theme.data.theme.title}`);
        this.userThemes = data.theme.data.UserThemes;
        this.relatedThemes = data.theme.data.relatedThemes;
        this.theme = data.theme.data.theme;
        this.loaderFinished = true;

      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.email, Validators.required]],
      comment: ['', [Validators.required, Validators.minLength(2)]],
    });
    if (this.theme.user.id.toString() === localStorage.getItem('id')) {
      this.theSameUser = true;
    }
    this.comment.getUserRate(this.theme.user.id).subscribe(value => this.userRate = value.rate);
    this.ads.getAds().subscribe(value => this.advs = value['data']);

  }
  ngAfterViewInit() {
    $('.pre-img').click(function () {
      $('#cover').attr('src', $(this).attr('src'));
      $('.pre-img').removeClass('active');
      $(this).addClass('active');
    });
  }
  parseNaviagtion(id: number): void {
    this.router.navigateByUrl(`themes/download/${+id}`);
  }
  openUrl(url: string) {
    location.href = url;
  }
  openDetails(id: number | string, title: string, type: string) {
    this.loaderFinished = false;
    this.router.navigate([`themes/${type}/${id}/${title}/${this.affiliate_code}`]);
  }
  addComment(): void {
    this.comment.addComments({
      name: this.newForm.get('name').value || this.name,
      email: this.newForm.get('email').value || this.email,
      comment: this.newForm.get('comment').value,
      type: 'theme',
      item_id: this.theme.id.toString()
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.commentStatus = true;
        setTimeout(() => {
          this.commentStatus = undefined;
          this.newForm.get('name').patchValue('');
          this.newForm.get('email').patchValue('');
          this.newForm.get('comment').patchValue('');
        }, 2000);
      } else {
        this.commentStatus = false;
      }
    });
  }
  addThemeRate(num: number) {
    this.comment.addRate({
      item_id: this.theme.id.toString(),
      type: 'theme',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeThemeRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.blog-starts').eq(i).addClass('rate');
    }
  }
  uncolorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'grey');
      $('.blog-starts').eq(i).removeClass('rate');
    }

  }
  addUserRate(num: number) {
    this.comment.addRate({
      item_id: this.theme.id.toString(),
      type: 'user',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeUserRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.user-starts').eq(i).addClass('rate');
    }
  }
  uncolorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'black');
      $('.user-starts').eq(i).removeClass('rate');
    }

  }
  openAd(url: string) {
    location.assign(url);
  }
  buyTheme(id: number) {
    this.auth.buyTheme(id).subscribe(value => window.open(value.toString(), '_blank', 'top=100,left=500,width=600,height=500'));
  }
  share(site: string) {
    switch (site) {
      case 'fb':
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'twitter':
        window.open(`https://twitter.com/home?status=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'google':
        window.open(`https://plus.google.com/share?url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'pin':
        window.open(`https://pinterest.com/pin/create/button/?url=${window.location.href}&media=${this.theme['image']}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      default:
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
    }
  }


}
