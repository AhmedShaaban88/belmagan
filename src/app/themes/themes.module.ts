import { NgModule } from '@angular/core';
import { SharedModule} from '../shared/shared.module';
import {ThemesRoutingModule} from './themes-routing.module';
import {ContainerComponent} from './container/container.component';
import {ThemesFreeComponent} from './components/free/themes/themes.component';
import {DetailsFreeComponent} from './components/free/details/details.component';
import {ThemesPaidComponent} from './components/paid/themes/themes.component';
import {DetailsPaidComponent} from './components/paid/details/details.component';
import {DownloadFreeComponent} from './components/free/download/download.component';
import {ResolveService, ResolveServiceCat, ResolveServiceCatPaid, ResolveServiceNotActive} from './resolve.service';
import {AuthService} from './auth.service';
import { CategoryFreeComponent } from './components/free/category-free/category-free.component';
import { PaidCategoryComponent } from './components/paid/paid-category/paid-category.component';
import { AllCategoriesComponent } from './components/free/all-categories/all-categories.component';
import {AllCategoriesPaidComponent} from './components/paid/all-categories-paid/all-categories.component';
import {DetailsViewComponent} from './components/details/details.component';

@NgModule({
  imports: [
    SharedModule,
    ThemesRoutingModule
  ],
  providers: [ResolveService, AuthService, ResolveServiceCat, ResolveServiceCatPaid, ResolveServiceNotActive],
  declarations: [ContainerComponent, ThemesFreeComponent, DetailsFreeComponent, ThemesPaidComponent, DetailsPaidComponent, DownloadFreeComponent, CategoryFreeComponent, PaidCategoryComponent, AllCategoriesComponent, AllCategoriesPaidComponent, DetailsViewComponent]
})
export class ThemesModule { }
