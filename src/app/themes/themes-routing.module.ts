import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContainerComponent} from './container/container.component';
import {ThemesFreeComponent} from './components/free/themes/themes.component';
import {DetailsFreeComponent} from './components/free/details/details.component';
import {ThemesPaidComponent} from './components/paid/themes/themes.component';
import {DetailsPaidComponent} from './components/paid/details/details.component';
import {DownloadFreeComponent} from './components/free/download/download.component';
import {ResolveService, ResolveServiceCat, ResolveServiceCatPaid, ResolveServiceNotActive} from './resolve.service';
import {CategoryFreeComponent} from './components/free/category-free/category-free.component';
import {PaidCategoryComponent} from './components/paid/paid-category/paid-category.component';
import {AllCategoriesComponent} from './components/free/all-categories/all-categories.component';
import {AllCategoriesPaidComponent} from './components/paid/all-categories-paid/all-categories.component';
import {DetailsViewComponent} from './components/details/details.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';

const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    children: [
      {
        path: '',
        component: ThemesFreeComponent,
        children: [
          {
            path: '',
            component: AllCategoriesComponent
          },
          {
            path: 'free/:cat',
            component: CategoryFreeComponent,
            resolve: {
              theme: ResolveServiceCat
            }
          }
        ]
      },
      {
        path: 'free/:id/:name/:affiliate_code',
        component: DetailsFreeComponent,
        resolve: {
          theme: ResolveService
        }
      },
      {
        path: 'download/:id',
        component: DownloadFreeComponent
      },
      {
        path: 'user/view/private/:id',
        component: DetailsViewComponent,
        canActivate: [AuthGaurdService],
        resolve: {
          theme: ResolveServiceNotActive
        }
      },
      {
        path: 'paid',
        component: ThemesPaidComponent,
        children: [
          {
            path: '',
            component: AllCategoriesPaidComponent
          },
          {
            path: ':cat',
            component: PaidCategoryComponent,
            resolve: {
              theme: ResolveServiceCatPaid
            }
          }
        ]
      },
      {
        path: 'paid/:id/:name/:affiliate_code',
        component: DetailsPaidComponent,
        resolve: {
          theme: ResolveService
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemesRoutingModule { }
