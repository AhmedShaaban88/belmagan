import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Title} from '@angular/platform-browser';
import {AuthService} from './auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.sass'],
  providers: [AuthService]
})
export class StatisticsComponent implements OnInit {
  pageTitle = 'الاحصائيات والمعاملات';
  statistics: any;
  depositBanks: any;
  withdrawBanks = [];
  myWithdraw: any;
  loaderFinished = false;
  loaderFinished2 = false;
  newForm: FormGroup;
  errorForm: string;
  affiliate_code = localStorage.getItem('affiliate_code');
  codeURL = `${location.host}/register/${this.affiliate_code}`;
  constructor(private title: Title, private auth: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getStatistics().subscribe(value => {
      this.statistics = value.data;
      this.loaderFinished2 = true;
    });
    this.auth.getDepositBanks().subscribe(value => this.depositBanks = value.data);
    this.auth.getWithdrawBanks().subscribe(value => {
      this.withdrawBanks = value.data;
      this.loaderFinished = true;

    });
    this.auth.getWithdrawTransactions().subscribe((value: any) => {
      this.myWithdraw = value.data;
    }
    );
    this.newForm = this.fb.group({
      bank: ['', [Validators.required]],
      money: ['', [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
    });
  }
   copy(): void {
     window.scrollTo(0, 0);
     $('#copy-input').select();
    document.execCommand('copy');
  }
  withDraw() {
    if (this.newForm.status === 'VALID' && this.newForm.get('money').value > 0) {
      this.errorForm = undefined;
      this.auth.sendWithdrawTransaction({
        amount: this.newForm.get('money').value.toString(),
        bank_id: this.newForm.get('bank').value
      }).subscribe((value: any) => {
        if (value.status === 'failed') {
          this.errorForm = value.data;
        } else {
          const x = $('#toast');
          x.addClass('show');
          x.html('تم طلب السحب بنجاح');
          setTimeout(function() {
            x.removeClass('show');
          }, 3000);
          this.newForm.setValue({
            money: '',
            bank: ''
          });
          this.auth.getWithdrawTransactions().subscribe((banks: any) => {
            this.myWithdraw = banks.data;
          });
        }
      });
    } else {
      this.errorForm = 'بعض الخانات فارغه او غير صحيحه';
    }
  }
  share(site: string) {
    switch (site) {
      case 'fb':
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${this.codeURL}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'twitter':
        window.open(`https://twitter.com/home?status=${this.codeURL}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'google':
        window.open(`https://plus.google.com/share?url=${this.codeURL}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'pin':
        window.open(`https://pinterest.com/pin/create/button/?url=${this.codeURL}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      default:
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${this.codeURL}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
    }
  }

}
