import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
   private api = 'http://dashboard.belmagan.com/api/MyStatistical';
   private depositeBanks = 'http://dashboard.belmagan.com/api/bank/deposit';
   private withdrawBanks = 'http://dashboard.belmagan.com/api/bank/withdraw';
   private myWithdraw = 'http://dashboard.belmagan.com/api/MyWithDraw';
   private withdrawTransactions = 'http://dashboard.belmagan.com/api/WithDraw';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getStatistics(): Observable<any> {
    return this.Http.get<any>(this.api).pipe(retry(3), catchError(this.handle.handleError));
  }
  getDepositBanks() {
    return this.Http.get<any>(this.depositeBanks).pipe(retry(3), catchError(this.handle.handleError));
  }
  getWithdrawBanks() {
    return this.Http.get<any>(this.withdrawBanks).pipe(retry(3), catchError(this.handle.handleError));
  }
  getWithdrawTransactions() {
    return this.Http.get(this.myWithdraw, {headers: new HttpHeaders({'Accept': 'application/json'})}).pipe(retry(3), catchError(this.handle.handleError));
  }
  sendWithdrawTransaction(body: object) {
    return this.Http.post(this.withdrawTransactions, body).pipe(retry(3), catchError(this.handle.handleError));
  }

}
