import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  socialMedia: any;
  footerPages: any;
  zoom = 15;
  lat = 31.0525829;
  lng = 31.4025308;
  constructor(private http: HttpClient) { }

  ngOnInit() {
     this.http.get<any>('http://dashboard.belmagan.com/api/setting').subscribe(value => this.socialMedia = value.data);
     this.http.get<any>('http://dashboard.belmagan.com/api/get/sections').subscribe(value => this.footerPages = value.data);
  }

}
