import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CoursesShellComponent} from './container/courses-shell/courses-shell.component';
import {UserCoursesComponent} from './components/user-courses/user-courses.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
const routes: Routes = [
  {
    path: '',
    component: CoursesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserCoursesComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserCoursesRoutingModule { }
