import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-themes',
  templateUrl: './user-courses.component.html',
  styleUrls: ['./user-courses.component.sass'],
  providers: [AuthService]
})
export class UserCoursesComponent implements OnInit {
  pageTitle = 'دوراتي التعليميه';
  myCourses: any;
  error: string;
  loaderFinished = false;
  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getMyCourses().subscribe(value => {
      if (value.data === null) {
        this.error = 'لا يوجد لديك اي دورات حتي الان !';
      }
      this.myCourses = value.data;
    }, error1 => this.error = error1, () => this.loaderFinished = true);
  }
  openCourse(title: string, id: number | string) {
    this.router.navigate([`../../learning/${title}/${+id}`]);
  }


}
