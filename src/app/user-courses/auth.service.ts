import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private apiMyCourses = 'http://dashboard.belmagan.com/api/MyCourses';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyCourses(): Observable<any> {
    return this.Http.get<any>(this.apiMyCourses).pipe(retry(3), catchError(this.handle.handleError));
  }

}
