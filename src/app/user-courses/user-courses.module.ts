import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserCoursesRoutingModule} from './user-courses-routing.module';

import { CoursesShellComponent } from './container/courses-shell/courses-shell.component';
import {UserCoursesComponent} from './components/user-courses/user-courses.component';


@NgModule({
  imports: [
    SharedModule,
    UserCoursesRoutingModule
  ],
  declarations: [UserCoursesComponent, CoursesShellComponent]
})
export class UserCoursesModule { }
