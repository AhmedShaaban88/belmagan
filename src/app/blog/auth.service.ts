import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Blog} from './components/blog-home/blogs';

@Injectable()
export class AuthService {
   private apiCateories = 'http://dashboard.belmagan.com/api/categories/blog';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getCateories(): Observable<any> {
    return this.Http.get<any>(this.apiCateories).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCategoryData(id: number | string, name: string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/categories/blog/${+id}/${name}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getBlogDetails(id: number | string, name: string, affiliate_code?: number | string): Observable<any> {
    if (affiliate_code !== 'details' && localStorage.getItem('affiliate_code')) {
      return this.Http.get<any>(`http://dashboard.belmagan.com/api/blog/${+id}/${name}/${affiliate_code}`).pipe(retry(3), catchError(this.handle.handleError));
    }
    return this.Http.get<Blog>(`http://dashboard.belmagan.com/api/blog/${+id}/${name}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getUserNonActiveBlog(id: number | string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/get/blog/${+id}`).pipe(retry(3), catchError(this.handle.handleError));

  }
  getNextPageCat(id: number | string , name: string, num: number) {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/categories/blog/${+id}/${name}?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));

  }

}
