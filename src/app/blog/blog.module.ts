import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogShellComponent } from './container/blog-shell/blog-shell.component';
import { BlogHomeComponent } from './components/blog-home/blog-home.component';
import { BlogComponent } from './components/blog/blog.component';
import {AuthService} from './auth.service';
import {ResolveService, ResolveServiceBlog, ResolveBlogNotActive} from './resolve.service';
import { BlogDetailsComponent } from './components/blog-details/blog-details.component';
import {DetailsViewComponent} from './components/details-user/details.component';

@NgModule({
  imports: [
    SharedModule,
    BlogRoutingModule
  ],
  providers: [AuthService, ResolveService, ResolveServiceBlog, ResolveBlogNotActive],
  declarations: [BlogShellComponent, BlogHomeComponent, BlogComponent, BlogDetailsComponent, DetailsViewComponent]
})
export class BlogModule { }
