import {Component, HostListener, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Blog} from '../blog-home/blogs';
import {AuthService} from '../../auth.service';


@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.sass'],
})
export class BlogDetailsComponent implements OnInit {
  blogs: Blog;
  pageTitle: string;
  failedMsg: string;
  loaderFinished = false;
  currentPage = 1;
  pageArticle = [];
  pageLoader = false;
  articleName: string;
  articleID: number;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';

  constructor(private title: Title, private route: ActivatedRoute, private router: Router, private auth: AuthService) {

    this.route.data.subscribe((data) => {
      this.pageTitle = data.blog.category.title;
      if (data.blog.data.blogs !== undefined) {
        this.blogs = data.blog.data.blogs;
        this.articleID = data.blog.category.id;
        this.articleName = data.blog.data.blogs[0].slug;
        this.loaderFinished = true;
        this.title.setTitle(` بالمجان - ${this.pageTitle}`);

      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; }, () => this.loaderFinished = true);
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`blog/${+id}/${title}/${this.affiliate_code}`]);
  }
  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPageCat(this.articleID , this.articleName , ++this.currentPage).subscribe((value: any) => {
        if (value.data.blogs > 0) {
          this.pageArticle.push(value.data.blogs);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }

}
