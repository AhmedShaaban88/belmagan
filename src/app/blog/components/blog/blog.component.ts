import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Blog} from '../blog-home/blogs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentsService} from '../../../shared/comment_rate.service';
import * as $ from 'jquery';
import {AdsService} from '../../../shared/ads.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.sass']
})
export class BlogComponent implements OnInit {
  blog: Blog;
  comments: any;
  user: any;
  userBlog: Blog[];
  relatedBlog: Blog[];
  failedMsg: string;
  loaderFinished = false;
  newForm: FormGroup;
  name = localStorage.getItem('name');
  email = localStorage.getItem('email');
  commentStatus = undefined;
  userRate: any;
  makeBlogRate = false;
  makeUserRate = false;
  theSameUser = false;
  advs: any;
  activeNow: boolean;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';

  constructor(private title: Title, private route: ActivatedRoute, private router: Router,
              private comment: CommentsService, private fb: FormBuilder, private ads: AdsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.blog.data.blog !== undefined) {
        this.blog = data.blog.data.blog;
        this.activeNow = data.blog.data.user_is_online;
        this.comments = data.blog.data.comments;
        this.user = data.blog.data.user;
        this.userBlog = data.blog.data.userblog;
        this.relatedBlog = data.blog.data.relatedblog;
        this.loaderFinished = true;
      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
    this.title.setTitle(` بالمجان - ${this.blog.title}`);
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.email, Validators.required]],
      comment: ['', [Validators.required, Validators.minLength(2)]],
    });
    if (this.user.id.toString() === localStorage.getItem('id')) {
      this.theSameUser = true;
    }
    this.comment.getUserRate(this.user.id).subscribe(value => this.userRate = value.rate);
    this.ads.getAds().subscribe(value => this.advs = value['data']);

  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    window.scrollTo(0 , 0);
    this.router.navigate([`blog/${+id}/${title}/${this.affiliate_code}`]);
  }
  addComment(): void {
    this.comment.addComments({
      name: this.newForm.get('name').value || this.name,
      email: this.newForm.get('email').value || this.email,
      comment: this.newForm.get('comment').value,
      type: 'blog',
      item_id: this.blog.id.toString()
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.commentStatus = true;
        setTimeout(() => {
          this.commentStatus = undefined;
          this.newForm.get('name').patchValue('');
          this.newForm.get('email').patchValue('');
          this.newForm.get('comment').patchValue('');
        }, 2000);
      } else {
        this.commentStatus = false;
      }
    });
  }
  addBlogRate(num: number) {
  this.comment.addRate({
    item_id: this.blog.id.toString(),
    type: 'blog',
    rate_no: num
  }).subscribe(value => {
    if (value.msg === 'تم بنجاح') {
      this.makeBlogRate = true;
      const x = document.getElementById('toast');
      x.classList.add('show');
      x.innerHTML = 'تم اضافه التقييم بنجاح';
      setTimeout(function() {
        x.classList.remove('show');
      }, 3000);
    }
  });
  }
  colorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.blog-starts').eq(i).addClass('rate');
    }
  }
  uncolorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'grey');
      $('.blog-starts').eq(i).removeClass('rate');
    }

  }
  addUserRate(num: number) {
    this.comment.addRate({
      item_id: this.blog.id.toString(),
      type: 'user',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeUserRate = true;
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم اضافه التقييم بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      }
    });
  }
  colorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.user-starts').eq(i).addClass('rate');
    }
  }
  uncolorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'black');
      $('.user-starts').eq(i).removeClass('rate');
    }

  }
  openAd(url: string) {
    location.assign(url);
  }
  share(site: string) {
    switch (site) {
      case 'fb':
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'twitter':
        window.open(`https://twitter.com/home?status=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'google':
        window.open(`https://plus.google.com/share?url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'pin':
        window.open(`https://pinterest.com/pin/create/button/?url=${window.location.href}&media=${this.blog['image']}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      default:
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
    }
  }

}
