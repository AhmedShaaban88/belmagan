import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../auth.service';
@Component({
  selector: 'app-blog-home',
  templateUrl: './blog-home.component.html',
  styleUrls: ['./blog-home.component.sass']
})
export class BlogHomeComponent implements OnInit {
  pageTitle = 'اقسام المدونه';
  categories: string;
  failedMsg: string;
  loaderFinished = false;
  constructor(private router: Router,  private title: Title, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(categories => {this.categories = categories.data.blogCategories}, error => {this.failedMsg = error; this.loaderFinished = true; } , () => this.loaderFinished = true);
  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`blog/category/${+id}`, title]);
  }

}
