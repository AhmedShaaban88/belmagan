import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AdsService} from '../../../shared/ads.service';

@Component({
  selector: 'app-home',
  templateUrl: './details.component.html',
  styleUrls: ['./detailscomponent.sass'],
})
export class DetailsViewComponent implements OnInit {
  blog: any;
  failedMsg: string;
  loaderFinished = false;
  advs: any;
  constructor(private router: Router, private title: Title, private route: ActivatedRoute, private ads: AdsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {

      if (data.blog !== undefined && data.blog.data !== 'غير موجوده') {
        this.title.setTitle(` بالمجان - ${data.blog.data.title}`);
        this.blog = data.blog.data;
        this.loaderFinished = true;

      } else {
        this.router.navigate(['not-found']);
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
    this.ads.getAds().subscribe(value => this.advs = value['data']);


  }
  openAd(url: string) {
    location.assign(url);
  }


}
