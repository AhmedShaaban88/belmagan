import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {Themes} from '../shared/Themes';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';

@Injectable()
export class ResolveService implements Resolve<Themes> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const blogID = route.paramMap.get('id');
    const blogName = route.paramMap.get('name');
    const affiliate_code = route.paramMap.get('affiliate_code') || 'details';

    return this.auth.getBlogDetails(blogID, blogName, affiliate_code).pipe(
     take(1),
     mergeMap(blog => {
       if (blog.status !== 'failed') {
         return of(blog);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
@Injectable()
export class ResolveServiceBlog implements Resolve<Themes> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any>| Promise<any> {
    const blogID = route.paramMap.get('blogID');
    const blogName = route.paramMap.get('blogName');

    return this.auth.getCategoryData(blogID, blogName).pipe(
      take(1),
      mergeMap(blog => {
        if (blog.status !== 'failed') {
          return of(blog);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}

@Injectable()
export class ResolveBlogNotActive implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Themes> | Observable<any> | Promise<any> {
    const blogID = route.paramMap.get('id');
    return this.auth.getUserNonActiveBlog(blogID).pipe(
      take(1),
      mergeMap(blog => {
        if (blog.status !== 'failed') {
          return of(blog);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}

