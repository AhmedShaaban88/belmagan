import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BlogShellComponent} from './container/blog-shell/blog-shell.component';
import {BlogHomeComponent} from './components/blog-home/blog-home.component';
import {BlogComponent} from './components/blog/blog.component';
import {ResolveBlogNotActive, ResolveService, ResolveServiceBlog} from './resolve.service';
import {BlogDetailsComponent} from './components/blog-details/blog-details.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {DetailsViewComponent} from './components/details-user/details.component';

const routes: Routes = [
  {
    path: '',
    component: BlogShellComponent,
    children: [
      {
        path: '',
        component: BlogHomeComponent
      },
      {
        path: 'category/:blogID/:blogName',
        component: BlogDetailsComponent,
        resolve: {
          blog: ResolveServiceBlog
        }
      },
      {
        path: ':id/:name/:affiliate_code',
        component: BlogComponent,
        resolve: {
          blog: ResolveService
        }
      },
      {
        path: 'user/view/private/:id',
        component: DetailsViewComponent,
        canActivate: [AuthGaurdService],
        resolve: {
          blog: ResolveBlogNotActive
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
