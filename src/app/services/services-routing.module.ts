import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ServicesShellComponent} from './container/services-shell/services-shell.component';
import {ServicesHomeComponent} from './components/services-home/services-home.component';
import {ServicesDetailsComponent} from './components/services-details/services-details.component';
import {AllCategoriesComponent} from './components/all-categories/all-categories.component';
import {ResolveService, ResolveServiceCat} from './resolve.service';
import {DetailsViewComponent} from './components/details-user/details.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {ResolveServiceNotActive} from './resolve.service';
import {DownloadServiceComponent} from './components/download/download.component';

const routes: Routes = [
  {
    path: '',
    component: ServicesShellComponent,
    children: [
      {
        path: '',
        component: AllCategoriesComponent
      },
      {
        path: ':id/:name',
        component: ServicesHomeComponent,
        resolve: {
          service: ResolveServiceCat
        }
      },
      {
        path: ':name/details/:id/:affiliate_code',
        component: ServicesDetailsComponent,
        resolve: {
          service: ResolveService
        }
      },
      {
        path: 'user/view/private/:id',
        component: DetailsViewComponent,
        canActivate: [AuthGaurdService],
        resolve: {
          service: ResolveServiceNotActive
        }
      },
      {
        path: 'download/service/:id',
        component: DownloadServiceComponent
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
