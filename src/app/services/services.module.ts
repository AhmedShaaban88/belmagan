import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { ServicesRoutingModule } from './services-routing.module';
import { ServicesShellComponent } from './container/services-shell/services-shell.component';
import { ServicesHomeComponent } from './components/services-home/services-home.component';
import { ServicesDetailsComponent } from './components/services-details/services-details.component';
import { AllCategoriesComponent } from './components/all-categories/all-categories.component';
import {DetailsViewComponent} from './components/details-user/details.component';
import {AuthService} from './auth.service';
import {ResolveService, ResolveServiceCat, ResolveServiceNotActive} from './resolve.service';
import {DownloadServiceComponent} from './components/download/download.component';

@NgModule({
  imports: [
    CommonModule,
    ServicesRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ServicesShellComponent, ServicesHomeComponent, ServicesDetailsComponent, AllCategoriesComponent, DetailsViewComponent, DownloadServiceComponent],
  providers: [AuthService, ResolveService, ResolveServiceCat, ResolveServiceNotActive]
})
export class ServicesModule { }
