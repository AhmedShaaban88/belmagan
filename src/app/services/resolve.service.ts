import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';
import {Themes} from '../shared/Themes';

@Injectable()
export class ResolveService implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any>| Promise<any> {
    const serviceID = route.paramMap.get('id');
    const serviceName = route.paramMap.get('name');
    const affiliate_code = route.paramMap.get('affiliate_code') || 'service';

    return this.auth.getServiceDetails(+serviceID, affiliate_code).pipe(
      take(1),
      mergeMap(service => {
        if (service.status !== 'failed') {
          return of(service);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
@Injectable()
export class ResolveServiceCat implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any>| Promise<any> {
    const serviceID = route.paramMap.get('id');

    return this.auth.getCatData(+serviceID).pipe(
      take(1),
      mergeMap(service => {
        if (service.status !== 'failed') {
          return of(service);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}

@Injectable()
export class ResolveServiceNotActive implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Themes> | Observable<any> | Promise<any> {
    const serviceID = route.paramMap.get('id');
    return this.auth.getUserNonActiveService(+serviceID).pipe(
      take(1),
      mergeMap(service => {
        if (service.status !== 'failed') {
          return of(service);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
