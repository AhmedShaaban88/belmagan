import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-services-shell',
  templateUrl: './services-shell.component.html',
  styleUrls: ['./services-shell.component.sass']
})
export class ServicesShellComponent implements OnInit {
  pageTitle = 'الخدمات';

  constructor(private title: Title) {

  }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

  }

}
