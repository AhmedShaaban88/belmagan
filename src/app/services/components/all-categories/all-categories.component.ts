import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import {Title} from '@angular/platform-browser';
import {Themes} from '../../../shared/Themes';

@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.sass']
})
export class AllCategoriesComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService, private title: Title) {
  }

  categories: any;
  allCount: number | string;
  services: any;
  loaderFinished = false;
  currentPage = 1;
  pageService = [];
  pageLoader = false;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'service';

  ngOnInit() {
    this.auth.getServicesCat().subscribe(categories => {
      this.categories = categories.data;
      this.allCount = categories.allCount;
    }, () => this.loaderFinished = true, () => this.loaderFinished = true);
    this.auth.getAllServices().subscribe(services => this.services = services.data.data);
    this.title.setTitle(` بالمجان - الخدمات`);
  }

  gotoLink(id: number | string, title): void {
    this.router.navigate([`services/${+id}/${title}`]);
  }

  gotoDetails(title: string, id: number | string): void {
    this.loaderFinished = false;
    this.router.navigate([`services/${title}/details/${+id}/${this.affiliate_code}`]);
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPageAll(++this.currentPage).subscribe((value: any) => {
        if (value.data.data.length > 0) {
          this.pageService.push(value.data.data);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }
}
