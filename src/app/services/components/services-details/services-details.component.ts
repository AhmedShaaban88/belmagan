import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as $ from 'jquery';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentsService} from '../../../shared/comment_rate.service';
import {AdsService} from '../../../shared/ads.service';

@Component({
  selector: 'app-services-details',
  templateUrl: './services-details.component.html',
  styleUrls: ['./services-details.component.sass']
})
export class ServicesDetailsComponent implements OnInit,  AfterViewInit {
    serviceTitle: string;
    error: string;
    loaderFinished = false;
    services: any;
    comments: any;
    user: any;
    relatedServices: any;
    userServices: any;
    newForm: FormGroup;
    name = localStorage.getItem('name');
    email = localStorage.getItem('email');
    commentStatus = undefined;
    userRate: any;
  makeServiceRate = false;
  makeUserRate = false;
  theSameUser = false;
  advs: any;
  activeNow: boolean;
  affiliate_code = localStorage.getItem('affiliate_code') || 'service';
  constructor(private title: Title, private auth: AuthService, private route: ActivatedRoute, private router: Router,
              private comment: CommentsService, private fb: FormBuilder, private ads: AdsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      this.serviceTitle = data.service.data.service.title;
      if (data.service.data !== undefined) {
        this.services = data.service.data.service;
        this.activeNow = data.service.data.user_is_online;
        this.comments = data.service.data.service.comments;
        this.user = data.service.data.user;
        this.userServices = data.service.data.userServices;
        this.relatedServices = data.service.data.relatedservice;
        this.loaderFinished = true;
        this.title.setTitle(` بالمجان - ${this.serviceTitle}`);
      } else {
        this.loaderFinished = true;
      }
    }, error => {this.loaderFinished = false; });
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.email, Validators.required]],
      comment: ['', [Validators.required, Validators.minLength(2)]],
    });
    if (this.user.id.toString() === localStorage.getItem('id')) {
      this.theSameUser = true;
    }
    this.comment.getUserRate(this.user.id).subscribe(value => this.userRate = value.rate);
    this.ads.getAds().subscribe(value => this.advs = value['data']);
  }
  ngAfterViewInit() {
    $('.pre-img').click(function () {
      $('#cover').attr('src', $(this).attr('src'));
      $('.pre-img').removeClass('active');
      $(this).addClass('active');
    });
  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    window.scrollTo(0 , 0);
    this.router.navigate([`services/${title}/details/${+id}/${this.affiliate_code}`]);
  }
  downloadService(id: number): void {
    this.loaderFinished = false;
    this.router.navigateByUrl(`services/download/service/${+id}`);
  }
  addComment(): void {
    this.comment.addComments({
      name: this.newForm.get('name').value || this.name,
      email: this.newForm.get('email').value || this.email,
      comment: this.newForm.get('comment').value,
      type: 'service',
      item_id: this.services.id.toString()
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.commentStatus = true;
        setTimeout(() => {
          this.commentStatus = undefined;
          this.newForm.get('name').patchValue('');
          this.newForm.get('email').patchValue('');
          this.newForm.get('comment').patchValue('');
        }, 2000);
      } else {
        this.commentStatus = false;
      }
    });
  }
  addServiceRate(num: number) {
    this.comment.addRate({
      item_id: this.services.id.toString(),
      type: 'service',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeServiceRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.blog-starts').eq(i).addClass('rate');
    }
  }
  uncolorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'grey');
      $('.blog-starts').eq(i).removeClass('rate');
    }

  }
  addUserRate(num: number) {
    this.comment.addRate({
      item_id: this.services.id.toString(),
      type: 'user',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeUserRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.user-starts').eq(i).addClass('rate');
    }
  }
  uncolorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'black');
      $('.user-starts').eq(i).removeClass('rate');
    }

  }
  openAd(url: string) {
    location.assign(url);
  }
  buyService(id: number) {
    this.auth.buyService(id).subscribe(value => window.open(value.toString(), '_blank', 'top=100,left=500,width=600,height=500'));
  }
  share(site: string) {
    switch (site) {
      case 'fb':
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'twitter':
        window.open(`https://twitter.com/home?status=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'google':
        window.open(`https://plus.google.com/share?url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'pin':
        window.open(`https://pinterest.com/pin/create/button/?url=${window.location.href}&media=${this.services.image}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      default:
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${window.location.href}&title=${this.serviceTitle}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
    }
  }

}
