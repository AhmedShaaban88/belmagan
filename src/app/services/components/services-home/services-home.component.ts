import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import {Title} from '@angular/platform-browser';
@Component({
  selector: 'app-services-home',
  templateUrl: './services-home.component.html',
  styleUrls: ['./services-home.component.sass']
})
export class ServicesHomeComponent implements OnInit {
  categories: any;
  categoryData: any;
  serviceTitle: string;
  loaderFinished = false;
  currentID: number | string;
  allCount: number | string;
  currentPage = 1;
  pageService = [];
  pageLoader = false;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'service';
  constructor(private router: Router, private title: Title, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.auth.getServicesCat().subscribe(categories => {this.categories = categories.data; this.allCount = categories.allCount; });
    this.route.data.subscribe((data) => {
      this.pageLoader = false;
      this.currentID = this.route.snapshot.paramMap.get('id');
      this.serviceTitle = data.service.services.name;
      if (data.service.data[0] !== undefined) {
        this.categoryData = data.service.data[0].data;
        this.loaderFinished = true;
      } else {
        this.loaderFinished = true;
      }
    }, error => {this.loaderFinished = false; });
  }
  gotoLink(id: number | string, title): void {
    this.router.navigate([`services/${+id}/${title}`]);
  }
  gotoDetails(title: string , id: number | string): void {
    this.loaderFinished = false;
    this.router.navigate([`services/${title}/details/${+id}/${this.affiliate_code}`]);
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPageCat(this.currentID , ++this.currentPage).subscribe((value: any) => {
        if (value.data[0].data.length > 0 && value.data[0]) {
          this.pageService.push(value.data[0].data);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }

}
