import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private catURL = 'http://dashboard.belmagan.com/api/service-sections';
  private allURL = 'http://dashboard.belmagan.com/api/AllServices';
  private buyServiceURL = 'http://dashboard.belmagan.com/api/paypal/pay/service';
  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  getServiceDetails(id: number | string, affiliate_code?: number | string): Observable<any> {
    if (affiliate_code !== 'service' && localStorage.getItem('affiliate_code')) {
      return this.http.get<any>(`http://dashboard.belmagan.com/api/service/${id}/${affiliate_code}`).pipe(retry(3), catchError(this.handle.handleError));
    }
    return this.http.get<any>(`http://dashboard.belmagan.com/api/service/${id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getServicesCat(): Observable<any> {
    return this.http.get<any>(this.catURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCatData(id: number | string): Observable<any> {
    return this.http.get<any>(`http://dashboard.belmagan.com/api/SubServices/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getAllServices(): Observable<any> {
    return this.http.get<any>(this.allURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getUserNonActiveService(id: number | string): Observable<any> {
    return this.http.get<any>(`http://dashboard.belmagan.com/api/get/service/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  buyService(item: number) {
    return this.http.post(this.buyServiceURL, {itemId: item}, {responseType: 'text'}).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPageAll(num: number) {
    return this.http.get<any>(`http://dashboard.belmagan.com/api/AllServices?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPageCat(id: number | string, num: number) {
    return this.http.get<any>(`http://dashboard.belmagan.com/api/SubServices/${+id}?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));

  }
}
