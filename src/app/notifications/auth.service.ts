import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
 private api = 'http://dashboard.belmagan.com/api/MyNotifications';
 private lastAPI = 'http://dashboard.belmagan.com/api/LastNotifications';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getAllNotification(): Observable<any> {
    return this.Http.get(this.api).pipe(retry(3), catchError(this.handle.handleError));
  }
  getLastNotifications() {
    return this.Http.get(this.lastAPI).pipe(retry(3), catchError(this.handle.handleError));

  }
  seeNotification(id: number | string) {
    return this.Http.get(`http://dashboard.belmagan.com/api/seeNotification/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }


}
