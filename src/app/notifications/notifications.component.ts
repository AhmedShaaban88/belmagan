import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-account-settings',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.sass'],
  providers: [AuthService]
})
export class NotificationsComponent implements OnInit {
  pageTitle = 'الاشعارات';
  noNotification = false;
  loaderFinished = false;
  notifications: any;
  lastNotifications: any;
  constructor(private title: Title, private fb: FormBuilder,
              private auth: AuthService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getAllNotification().subscribe(value => {
      if (value.data.length === 0) {
        this.noNotification = true;
      } else {
        this.notifications = value.data;
        this.notifications = this.notifications.reverse();
      }
      this.loaderFinished = true;
    });
    this.auth.getLastNotifications().subscribe((value: any) => {
        this.lastNotifications = value.data.notifications;
        this.lastNotifications = this.lastNotifications.reverse();
    });

  }
  goToLink(id: number | string, type: string, seenID: number | string, name?: string) {
    this.loaderFinished = false;
    let ItemType;
    switch (type) {
      case 'theme':
        this.auth.seeNotification(seenID).subscribe(seen => seen);
        this.http.get(`http://dashboard.belmagan.com/api/get/theme/${+id}`).subscribe(value => {
          ItemType = value['data'].type;
          this.router.navigate([`themes/${ItemType}/${id}/${name}`]);
        });
        break;
      case 'service':
        this.auth.seeNotification(seenID).subscribe(seen => seen);
        this.http.get(`http://dashboard.belmagan.com/api/get/service/${+id}`).subscribe(value => {
          this.router.navigate([`services/${name}/details/${id}`]);
        });
        break;
      default:
        this.auth.seeNotification(seenID).subscribe(seen => seen);
        this.router.navigate([`user/statistics`]);
        break;
    }



  }

}
