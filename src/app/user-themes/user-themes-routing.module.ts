import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserThemesShellComponent} from './container/user-themes-shell/user-themes-shell.component';
import {UserThemesComponent} from './components/user-themes/user-themes.component';
import {UserThemesEditComponent} from './components/user-themes-edit/user-themes-edit.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {ResolveService} from './resolve.service';
import {UserThemesAddComponent} from './components/user-themes-add/user-themes-add.component';
import {UserThemesActiveComponent} from './components/user-themes-active/user-themes-active.component';
import {UserThemesNotActiveComponent} from './components/user-themes-not-active/user-themes-not-active.component';
const routes: Routes = [
  {
    path: '',
    component: UserThemesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserThemesComponent,
        children: [
          {
            path: '',
            component: UserThemesAddComponent
          },
          {
            path: 'active',
            component: UserThemesActiveComponent
          },
          {
            path: 'not-active',
            component: UserThemesNotActiveComponent
          }
        ]
      },
      {
        path: 'paid/edit/:id/:name',
        component: UserThemesEditComponent,
        resolve: {
          theme: ResolveService
        }
      },
      {
        path: 'free/edit/:id/:name',
        component: UserThemesEditComponent,
        resolve: {
          theme: ResolveService
        }
      },


    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserThemesRoutingModule { }
