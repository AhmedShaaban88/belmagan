import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesShellComponent } from './user-themes-shell.component';

describe('BlogThemesShellComponent', () => {
  let component: UserThemesShellComponent;
  let fixture: ComponentFixture<UserThemesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
