import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesAddComponent } from './user-themes-add.component';

describe('UserThemesAddComponent', () => {
  let component: UserThemesAddComponent;
  let fixture: ComponentFixture<UserThemesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
