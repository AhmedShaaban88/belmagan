import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-themes-active',
  templateUrl: './user-themes-active.component.html',
  styleUrls: ['./user-themes-active.component.sass']
})
export class UserThemesActiveComponent implements OnInit {
  pageTitle = 'القوالب النشطه';
  myThemesActiveFree = [];
  myThemesActivePaid = [];
  errorThemesActive = undefined;
  categories = [];
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';
  loaderFinished = false;

  constructor(private title: Title,  private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getThemesCat().subscribe(value => this.categories = value.data);
    this.auth.getMyThemes().subscribe(value => {this.filterThemes(value.data.themes);  this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(type: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/themes/${type}/edit/${+id}/${name}`]);
  }
  gotoTheme(type: string, id: number | string, name: string) {
    this.loaderFinished = false;

    this.router.navigate([`themes/${type}/${id}/${name}/${this.affiliate_code}`]);
  }
  filterThemes(themes: any) {
    for (let theme of themes) {
      if (theme.confirmed === '1' && theme.type === 'free') {
        this.myThemesActiveFree.push(theme);
      } else  if (theme.confirmed === '1' && theme.type === 'paid') {
        this.myThemesActivePaid.push(theme);
      }
    }
    if (this.myThemesActivePaid.length === 0 && this.myThemesActiveFree.length === 0){
      this.errorThemesActive = 'لا يوجد لديك اي قوالب حتي الان !';
    }

  }
  deleteTheme(themes: any, id: number | string, currentIndex: number) {
    const conf =  confirm('حذف القالب ؟');
    if (conf) {
      this.auth.deleteTheme(id).subscribe(value =>  {
        themes.splice(currentIndex, 1);
        if (this.myThemesActivePaid.length === 0 && this.myThemesActiveFree.length === 0) {
          this.errorThemesActive = 'لا يوجد لديك اي قوالب حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }


}
