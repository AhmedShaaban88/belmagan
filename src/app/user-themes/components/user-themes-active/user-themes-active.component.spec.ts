import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesActiveComponent } from './user-themes-active.component';

describe('UserThemesActiveComponent', () => {
  let component: UserThemesActiveComponent;
  let fixture: ComponentFixture<UserThemesActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
