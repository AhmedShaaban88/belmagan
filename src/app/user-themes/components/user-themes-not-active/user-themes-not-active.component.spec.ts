import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesNotActiveComponent } from './user-themes-not-active.component';

describe('UserThemesNotActiveComponent', () => {
  let component: UserThemesNotActiveComponent;
  let fixture: ComponentFixture<UserThemesNotActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesNotActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesNotActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
