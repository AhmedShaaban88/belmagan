import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-themes-not-active',
  templateUrl: './user-themes-not-active.component.html',
  styleUrls: ['./user-themes-not-active.component.sass']
})
export class UserThemesNotActiveComponent implements OnInit {
  pageTitle = 'القوالب الغير نشطه';
  myThemesNonActiveFree = [];
  myThemesNonActivePaid = [];
  errorThemesNonActive = undefined;
  categories = [];
  loaderFinished = false;
  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getThemesCat().subscribe(value => this.categories = value.data);
    this.auth.getMyThemes().subscribe(value => {this.filterThemes(value.data.themes);  this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(type: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/themes/${type}/edit/${+id}/${name}`]);
  }

  filterThemes(themes: any) {
    for (let theme of themes) {
      if (theme.confirmed === '0' && theme.type === 'free') {
        this.myThemesNonActiveFree.push(theme);
      } else {
        this.myThemesNonActivePaid.push(theme);
      }
    }
    if (this.myThemesNonActiveFree.length === 0 && this.myThemesNonActivePaid.length === 0){
      this.errorThemesNonActive = 'لا يوجد لديك اي قوالب حتي الان !';
    }

  }
  deleteTheme(themes: any, id: number | string, currentIndex: number) {
    const conf =  confirm('حذف القالب ؟');
    if (conf) {
      this.auth.deleteTheme(id).subscribe(value =>  {
        themes.splice(currentIndex, 1);
        if (this.myThemesNonActiveFree.length === 0 && this.myThemesNonActivePaid.length === 0) {
          this.errorThemesNonActive = 'لا يوجد لديك اي قوالب حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoThemeNonActive(id: string | number) {
    this.loaderFinished = false;

    this.router.navigate([`themes/user/view/private/${+id}`]);
  }

}
