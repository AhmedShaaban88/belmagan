import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-themes',
  templateUrl: './user-themes.component.html',
  styleUrls: ['./user-themes.component.sass'],
})
export class UserThemesComponent implements OnInit {
  pageTitle = 'قالب جديده';
  constructor(private title: Title, private router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
  }
  openURL(url: string) {
    if (url === '') {
      this.router.navigate(['user/themes']);
    } else {
      this.router.navigate(['user/themes', url]);
    }
  }


}

