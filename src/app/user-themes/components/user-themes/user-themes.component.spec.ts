import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesComponent } from './user-themes.component';

describe('UserBlogComponent', () => {
  let component: UserThemesComponent;
  let fixture: ComponentFixture<UserThemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
