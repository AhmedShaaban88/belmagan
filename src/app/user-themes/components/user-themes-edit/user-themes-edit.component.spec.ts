import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserThemesEditComponent } from './user-themes-edit.component';

describe('UserBlogEditComponent', () => {
  let component: UserThemesEditComponent;
  let fixture: ComponentFixture<UserThemesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserThemesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserThemesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
