import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-themes-edit',
  templateUrl: './user-themes-edit.component.html',
  styleUrls: ['./user-themes-edit.component.sass'],
})
export class UserThemesEditComponent implements OnInit {
  pageTitle = 'تعديل قالب';
  newForm: FormGroup;
  imageError: string;
  imagesError: string;
  images = [];
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  imagesData: Array<File> = [];
  rarFile: any;
  advantagesKeys = [];
  advantagesValues = [];
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];
  currentThemeDetails: any;
  hideFileDownload = false;
  themeID: any;
  themeCategory = undefined;
  changeMainImage = false;
  changeFile = false;
  changeImages = false;
  constructor(private title: Title, private fb: FormBuilder, private auth: AuthService,
              private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getThemesCat().subscribe(value => this.categories = value.data, () =>  null,() => {
      this.categories.filter((f, index) => {
        if (this.themeID === f.id.toString()) {
          this.categories.splice(index, 1);
          this.themeCategory = f.title;
        }
      });
    } );
    this.route.data.subscribe((data) => {
      if (data.theme !== undefined) {
        this.themeID = data.theme.data.category_id;
        this.currentThemeDetails = data.theme.data;
        this.newForm = this.fb.group({
          name: [this.currentThemeDetails.title, [Validators.required, Validators.minLength(3)]],
          desc: [this.currentThemeDetails.description, [Validators.required, Validators.minLength(8)]],
          content: [this.currentThemeDetails.content, [Validators.required, Validators.minLength(8)]],
          url: [this.currentThemeDetails.url, [Validators.required]],
          features: this.fb.array([
            this.fb.control(this.currentThemeDetails.advantages[0].title, [Validators.required, Validators.minLength(3)]),
            this.fb.control(this.currentThemeDetails.advantages[0].value, [Validators.required, Validators.minLength(3)])
          ]),
          type: [this.currentThemeDetails.type,  Validators.required],
          support: [this.currentThemeDetails.supply,  Validators.required],
          cost: [this.currentThemeDetails.cost, [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]]
        });
      }
      });
   this.allFeatures();
    this.imageUrl = this.currentThemeDetails.image;
    this.imageData = new File([this.currentThemeDetails.image], this.currentThemeDetails.image.name);
    this.rarFile = new File([this.currentThemeDetails.file], this.currentThemeDetails.file.name);
    this.currentThemeDetails.images.forEach(image => {this.images.push(image); this.imagesData.push(new File([image], image.name)); });
  }
  get features() {
    return this.newForm.get('features') as FormArray;
  }
  addFeature() {
    this.features.push(this.fb.control('', Validators.required));
    this.features.push(this.fb.control('', Validators.required));
  }
  allFeatures() {
    if (this.currentThemeDetails.advantages.length > 1) {
      for (let i = 1; i < this.currentThemeDetails.advantages.length; i++) {
        this.features.push(this.fb.control(this.currentThemeDetails.advantages[i].title, Validators.required));
        this.features.push(this.fb.control(this.currentThemeDetails.advantages[i].value, Validators.required));
      }
    }
  }

  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
          this.changeMainImage = true;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  selectImages(input: HTMLInputElement, event: any) {
    for (let i = 0; i < event.target.files.length; i++) {
      if ((event.target.files[i].type.indexOf('image') !== -1)) {
        if (Math.round(event.target.files[i].size / 1024) < 3000) {
          this.imagesError = undefined;
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.images.push(e.target.result);
            this.imagesData.push(event.target.files[i]);
            this.changeImages = true;
          };
          reader.readAsDataURL(event.target.files[i]);
        } else {
          this.imagesError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';
        }
      } else {
        this.imagesError = 'لابد ان تكون الصور بصيغه png. او jpeg.';
      }
    }
  }
  uploadRAR(files: HTMLInputElement) {
    this.hideFileDownload = true;
    this.rarFile = files.files[0];
    this.changeFile = true;
  }


  remove(item: any) {
    this.images.splice(item, 1);
    this.imagesData.splice(item, 1);

  }
  removeFeature(item: any) {
    this.features.removeAt(item - 1);
    this.features.removeAt(item);
    this.features.removeAt(item + 1);

  }
  newSubmit(): void {
    this.advantagesKeys = [];
    this.advantagesValues = [];
    this.features.controls.forEach((value, index ) => {if (index % 2 === 0) {
      this.advantagesKeys.push(value.value);
    } else {
      this.advantagesValues.push(value.value);
    }
    });
    if (this.newForm.get('type').value === 'free') {
      this.newForm.get('cost').patchValue('0');
    }
    if (this.newForm.status === 'VALID') {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      if (this.changeMainImage) {
        form.append('image', this.imageData);
      }
      if (this.changeFile) {
        form.append('file', this.rarFile);
      }
      form.append('title', this.newForm.get('name').value);
      form.append('description', this.newForm.get('desc').value);
      form.append('url', this.newForm.get('url').value);
      form.append('supply', this.newForm.get('support').value);
      form.append('type', this.newForm.get('type').value);
      form.append('cost', this.newForm.get('cost').value);
      form.append('category_id', $('#category option:selected' ).val());
      form.append('content', this.newForm.get('content').value);
      if (this.changeImages) {
      this.imagesData.forEach(f => form.append('images[]', f, f.name));
      }
      this.advantagesKeys.forEach(f => form.append('advantages[]', f));
      this.advantagesValues.forEach(f => form.append('values[]', f));
      this.auth.updateTheme(this.currentThemeDetails.id, form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تم حفظ التعديل بنجاح') {
          const x = document.getElementById('toast');
          this.successNew = true;
          x.classList.add('show');
          x.innerHTML = 'تم حفظ التعديل بنجاح';
          setTimeout(function() {
            x.classList.remove('show');
          }, 3000);
          this.router.navigate(['user/themes']);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
