import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Themes} from '../shared/Themes';

const headers = new HttpHeaders({
  'Accept': '/',
  'Access-Control-Allow-Methods': '*',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Credentials': 'true'

});

@Injectable()
export class AuthService {
  private apiMyThemes = 'http://dashboard.belmagan.com/api/MyThemes';
  private addNewTheme = 'http://dashboard.belmagan.com/api/add/theme';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyThemes(): Observable<any> {
    return this.Http.get<Themes>(this.apiMyThemes).pipe(retry(3), catchError(this.handle.handleError));
  }
  deleteTheme(id: number | string) {
    return this.Http.post(`http://dashboard.belmagan.com/api/delete/theme/${+id}`, {}).pipe(retry(3), catchError(this.handle.handleError));
  }
  getThemeDetails(id: number | string): Observable<any> {
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/get/theme/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  updateTheme(id: number | string, body: object) {
    return this.Http.post(`http://dashboard.belmagan.com/api/update/theme/${+id}`, body, {headers: headers}).pipe(retry(0), catchError(this.handle.handleError));
  }
  addTheme(body: object) {
    return this.Http.post(this.addNewTheme, body, {headers: headers}).pipe(retry(0), catchError(this.handle.handleError));
  }
  getThemesCat(): Observable<any> {
    return this.Http.get<Themes>('http://dashboard.belmagan.com/api/theme-categories').pipe(retry(3), catchError(this.handle.handleError));
  }

}
