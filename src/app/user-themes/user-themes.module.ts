import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserThemesRoutingModule} from './user-themes-routing.module';
import { UserThemesShellComponent } from './container/user-themes-shell/user-themes-shell.component';
import {UserThemesComponent} from './components/user-themes/user-themes.component';
import { UserThemesEditComponent } from './components/user-themes-edit/user-themes-edit.component';
import {AuthService} from './auth.service';
import {ResolveService} from './resolve.service';
import { UserThemesAddComponent } from './components/user-themes-add/user-themes-add.component';
import { UserThemesActiveComponent } from './components/user-themes-active/user-themes-active.component';
import { UserThemesNotActiveComponent } from './components/user-themes-not-active/user-themes-not-active.component';



@NgModule({
  imports: [
    SharedModule,
    UserThemesRoutingModule,
  ],
  declarations: [UserThemesComponent, UserThemesShellComponent, UserThemesEditComponent, UserThemesAddComponent, UserThemesActiveComponent, UserThemesNotActiveComponent],
  providers: [AuthService, ResolveService]
})
export class UserThemesModule { }
