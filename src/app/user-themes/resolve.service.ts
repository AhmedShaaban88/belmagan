import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';
import {Themes} from '../shared/Themes';

@Injectable()
export class ResolveService implements Resolve<Themes> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Themes> | Observable<any> | Promise<any> {
    const themeID = route.paramMap.get('id');
    const themeName = route.paramMap.get('name');

    return this.auth.getThemeDetails(themeID).pipe(
     take(1),
     mergeMap(theme => {
       if (theme.status !== 'failed') {
         return of(theme);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
