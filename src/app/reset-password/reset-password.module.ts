import { NgModule } from '@angular/core';
import { ResetPasswordRoutingModule } from './reset-password-routing.module';
import { ResetHomeComponent } from './component/reset-home/reset-home.component';
import { ResetUserComponent } from './component/reset-user/reset-user.component';
import { ResetContainerComponent } from './container/reset-container/reset-container.component';
import {AuthService} from './auth.service';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ResetPasswordRoutingModule
  ],
  declarations: [ResetHomeComponent, ResetUserComponent, ResetContainerComponent],
  providers: [AuthService]
})
export class ResetPasswordModule { }
