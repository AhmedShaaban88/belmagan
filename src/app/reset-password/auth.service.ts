import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Observable} from 'rxjs';
import {Users} from '../shared/users';

@Injectable()
export class AuthService {
  private passwordRequest = 'http://dashboard.belmagan.com/api/password/create';
  private resetRequest = 'http://dashboard.belmagan.com/api/password/reset';
  private apiUrl = 'http://dashboard.belmagan.com/api/auth/login';

  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  requestPassword(body: object) {
    return this.http.post<any>(this.passwordRequest, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  resetPassword(body: object) {
    return this.http.post<any>(this.resetRequest, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  login(data: Object): Observable<any> {
    return this.http.post<Users>(this.apiUrl, data).pipe(retry(3), catchError(this.handle.handleError));
  }
}
