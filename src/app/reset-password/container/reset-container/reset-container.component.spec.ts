import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetContainerComponent } from './reset-container.component';

describe('UserTicketsContainerComponent', () => {
  let component: ResetContainerComponent;
  let fixture: ComponentFixture<ResetContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
