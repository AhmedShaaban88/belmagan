import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-reset-home',
  templateUrl: './reset-home.component.html',
  styleUrls: ['./reset-home.component.sass']
})
export class ResetHomeComponent implements OnInit {
  pageTitle = 'استعاده كلمه المرور';
  successReset = undefined;
  email: string;
  errorMail: string;
  constructor(private auth: AuthService) { }

  ngOnInit() {

  }
  sendRequest() {
    if (this.email !== undefined) {
      this.errorMail = undefined;
      this.successReset = undefined;
      this.auth.requestPassword({email: this.email}).subscribe(value => {
        if (value.status === 'false') {
          this.successReset = false;
          this.errorMail = value.msg;
        } else {
          this.successReset = true;
          this.errorMail = undefined;
        }
      });
    } else {
      this.successReset = false;
      this.errorMail = 'بعض الخانات فارغه او غير صحيحه';
    }
  }

}
