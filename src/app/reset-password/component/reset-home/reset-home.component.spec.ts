import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetHomeComponent } from './reset-home.component';

describe('UserTicketsHomeComponent', () => {
  let component: ResetHomeComponent;
  let fixture: ComponentFixture<ResetHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
