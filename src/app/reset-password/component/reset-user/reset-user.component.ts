import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-reset-user',
  templateUrl: './reset-user.component.html',
  styleUrls: ['./reset-user.component.sass']
})
export class ResetUserComponent implements OnInit {
  private token: string;
  private email: string;
  pageTitle = 'استعاده كلمه المرور';
  successReset = undefined;
  errorMail: string;
  password: string;
  constructor(private route: ActivatedRoute, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.token = this.route.snapshot.queryParamMap.get('token');
    this.email = this.route.snapshot.queryParamMap.get('email');
  }
  resetPassword() {
    if (this.password !== undefined) {
      this.errorMail = undefined;
      this.successReset = undefined;
      this.auth.resetPassword({token: this.token, email: this.email, password: this.password}).subscribe(value => {
        if (value.status === 'false') {
          this.successReset = false;
          this.errorMail = value.msg;
        } else {
          this.successReset = true;
          this.errorMail = undefined;
          this.auth.login({email: this.email, password: this.password}).subscribe(value => {
            if (value.status === 'true') {
              localStorage.setItem('token', value.data.token);
              localStorage.setItem('id', value.data.user.id);
              localStorage.setItem('name', value.data.user.name);
              localStorage.setItem('email', value.data.user.email);
              localStorage.setItem('phone', value.data.user.phone);
              localStorage.setItem('affiliate_code', value.data.user.affiliate_code);
              setTimeout(() => {
                location.assign('/');
              }, 500);

            }
          });
        }
      });
    } else {
      this.successReset = false;
      this.errorMail = 'بعض الخانات فارغه او غير صحيحه';
    }
  }

}
