import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResetContainerComponent} from './container/reset-container/reset-container.component';
import {ResetHomeComponent} from './component/reset-home/reset-home.component';
import {ResetUserComponent} from './component/reset-user/reset-user.component';

const routes: Routes = [
  {
    path: '',
    component: ResetContainerComponent,
    children: [
      {
        path: '',
        component: ResetHomeComponent
      },
      {
        path: 'password',
        component: ResetUserComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetPasswordRoutingModule { }
