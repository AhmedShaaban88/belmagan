import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import * as $ from 'jquery';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {SearchService} from './search.service';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  pageTitle = 'الرئيسيه';
  search$ = new Subject<string>();
  searchResult$: Observable<any>;
  blogData: any;
  articleData: any;
  latest: any;
  download: any;
  views: any;
  viewLatest = true;
  viewDownload = false;
  viewMost = false;
  services: any;
  courses: any;
  loaderFinished = false;
  searchType = 'theme';
  exactlyUser = localStorage.getItem('token');
  value: any;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';
  affiliate_codeService = localStorage.getItem('affiliate_code') || 'service';
  affiliate_codeArticle = localStorage.getItem('affiliate_code') || 'article';

  constructor(private title: Title, private sservice: SearchService, private auth: AuthService
    , private router: Router) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.searchResult$ = this.search$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(value => this.sservice.search(value, this.searchType))
    );
    this.auth.getLatestThemes().subscribe(themes => this.latest = themes.data.themes, error => {
      const x = document.getElementById('toast');
      x.classList.add('show');
      x.innerHTML = error;
    }, () => this.loaderFinished = true);
    this.auth.getBlogs().subscribe(blogs => this.blogData = blogs.data);
    this.auth.getArticles().subscribe(articles => this.articleData = articles.data);
    this.auth.getServices().subscribe(services => this.services = services.data);
    this.auth.getCourses().subscribe(courses => this.courses = courses.data);
  }

  smoothScroll() {
    $('body, html').animate({
      scrollTop: $('#themes').offset().top
    }, 800);
  }

  search(text: string) {
    this.search$.next(text);

  }

  latestThemes() {
    this.auth.getLatestThemes().subscribe(themes => {
      this.latest = themes.data.themes;
      this.viewLatest = true;
      this.viewDownload = false;
      this.viewMost = false;
    });

  }

  downloadThemes() {

    this.auth.getDownloadThemes().subscribe(themes => {
      this.download = themes.data.themes;
      this.viewLatest = false;
      this.viewDownload = true;
      this.viewMost = false;
    });
  }

  viewThemes() {

    this.auth.getViewedThemes().subscribe(themes => {
      this.views = themes.data.themes;
      this.viewLatest = false;
      this.viewDownload = false;
      this.viewMost = true;
    });
  }

  slugifyThemes(id: number, type: string, title: string) {
    this.router.navigate([`themes/${type}/${id}/${title}/${this.affiliate_code}`]);
  }

  slugifyServices(id: number, title: string) {
    this.router.navigate([`services/${id}/${title}`]);
  }
  slugifyArticles(id: number, title: string) {
    this.router.navigate([`articles/${id}/${title}/${this.affiliate_codeArticle}`]);
  }

  slugOthers(section: string, id: number, title: string) {
    this.router.navigate([`${section}/${+id}/${title}/${this.affiliate_code}`]);
  }

  navigateSearch(id: number | string, title: string, theme_type = null) {
    switch (this.searchType) {
      case 'video':
        this.router.navigate([`articles/${+id}/${title}/${this.affiliate_codeArticle}`]);
        break;
      case 'service':
        this.router.navigate([`services/${title}/details/${+id}/${this.affiliate_codeService}`]);
        break;
      case 'blog':
        this.router.navigate([`blog/${+id}/${title}/${this.affiliate_code}`]);
        break;
      default:
        if (theme_type === 'free') {
          this.router.navigate([`themes/${theme_type}/${+id}/${title}/${this.affiliate_code}`]);
        } else {
          if (theme_type === 'paid') {
            this.router.navigate([`themes/${theme_type}/${+id}/${title}/${this.affiliate_code}`]);
          }
          break;
        }
    }

  }




}
