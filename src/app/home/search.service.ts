import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

export const searchUrl = 'http://dashboard.belmagan.com/api/search';


function createHttpOptions(searchValue: string, typeSearch: string) {
  const params = new HttpParams({ fromObject: { q: searchValue, type:  typeSearch} });
  const headers = new HttpHeaders({'x-refresh': 'true'}) ;
  return { headers, params };
}

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  search(searchValue: string, searchType: string): Observable<any> {
    if (!searchValue.trim()) { return of([]); }
    const options = createHttpOptions(searchValue, searchType);
    return this.http.get(searchUrl, options).pipe(
      map((data: any) => {
         return data.data.data;
      }),
      catchError(this.handle.handleError)
    );
  }
}
