import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Blog} from '../blog/components/blog-home/blogs';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {Articles} from '../articles/components/articles-home/articles';
import {Themes} from '../shared/Themes';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private blogURL = 'http://dashboard.belmagan.com/api/HomeBlog';
  private articleURL = 'http://dashboard.belmagan.com/api/HomeVideo';
  private latestThemes = 'http://dashboard.belmagan.com/api/latest-themes';
  private downloadThemes = 'http://dashboard.belmagan.com/api/Downloaded-more';
  private viewedThemes = 'http://dashboard.belmagan.com/api/viewed-more';
  private servicesURL = 'http://dashboard.belmagan.com/api/services';
  private coursesURL = 'http://dashboard.belmagan.com/api/HomeCourses';

  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  getBlogs(): Observable<any> {
    return this.http.get<Blog>(this.blogURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getArticles(): Observable<any> {
    return this.http.get<Articles>(this.articleURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getLatestThemes(): Observable<any> {
    return this.http.get<Themes>(this.latestThemes).pipe(retry(3), catchError(this.handle.handleError));
  }
  getDownloadThemes(): Observable<any> {
    return this.http.get<Themes>(this.downloadThemes).pipe(retry(3), catchError(this.handle.handleError));
  }
  getViewedThemes(): Observable<any> {
    return this.http.get<Themes>(this.viewedThemes).pipe(retry(3), catchError(this.handle.handleError));
  }
  getServices(): Observable<any> {
    return this.http.get<any>(this.servicesURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCourses(): Observable<any> {
    return this.http.get<any>(this.coursesURL).pipe(retry(3), catchError(this.handle.handleError));
  }

}
