import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Users} from '../shared/users';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private apiUrlProfile = 'http://dashboard.belmagan.com/api/update/profile';
  private apiUrlProfilePassword = 'http://dashboard.belmagan.com/api/update/password';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  updateProfile(data: Object): Observable<any> {
    return this.Http.post<Users>(this.apiUrlProfile, data).pipe(retry(3), catchError(this.handle.handleError));
  }
  updateProfilePassword(data: Object): Observable<any> {
    return this.Http.post<Users>(this.apiUrlProfilePassword, data).pipe(retry(3), catchError(this.handle.handleError));
  }

}
