import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';


@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.sass'],
  providers: [AuthService]
})
export class AccountSettingsComponent implements OnInit {
  pageTitle = 'اعدادات الحساب';
  accountForm: FormGroup;
  successAccount = undefined;
  error: string;
  errorPassword: string;
  passwordForm: FormGroup;
  successPassword = undefined;
  constructor(private title: Title, private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.accountForm = this.fb.group({
      name: [localStorage.getItem('name'), [Validators.required, Validators.minLength(3), Validators.pattern('^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$')]],
      email: [localStorage.getItem('email'), [Validators.required, Validators.email, Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')]],
      phone: [localStorage.getItem('phone'), [Validators.required, Validators.pattern('^[\+]?[(]?[0-9]{3,}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')]],
      bio: [localStorage.getItem('bio') || '', [Validators.required]]

  });
    this.passwordForm = this.fb.group({
      oldPassword: ['', [Validators.required, Validators.minLength(8), Validators.pattern('(?=.*[a-zA-Z\\\u0621-\\\u064A])(?=.*[0-9\\\u0660-\\\u0669])[a-zA-Za-z\\\u0621-\\\u064A0-9\\\u0660-\\\u0669]{8,}$')]],
      newPassword: ['', [Validators.required, Validators.minLength(8), Validators.pattern('(?=.*[a-zA-Z\\\u0621-\\\u064A])(?=.*[0-9\\\u0660-\\\u0669])[a-zA-Za-z\\\u0621-\\\u064A0-9\\\u0660-\\\u0669]{8,}$')]],
      reNewPassword: ['', [Validators.minLength(8), Validators.required]]
    });


  }
  accountSubmit(): void {
    if (this.accountForm.status === 'VALID') {
      this.auth.updateProfile(this.accountForm.value).subscribe(value => {
        if (value.status === 'done') {
         localStorage.setItem('name', value.data.name);
          localStorage.setItem('email', value.data.email);
          localStorage.setItem('phone', value.data.phone);
          localStorage.setItem('bio', value.data.bio);

          this.successAccount = true;
          this.error = undefined;
        } else {
            this.successAccount  = false;
            this.error = value.data;
        }
      }, error1 => {this.successAccount = false; this.error = error1; });
    } else {
      this.successAccount = false;
      this.error = 'بعض الخانات فارغه او غير صحيحه';
    }

  }
  passwordSubmit(): void {
    if (this.passwordForm.status === 'VALID' && this.checkPassword()) {
      this.auth.updateProfilePassword({
        curPassword: this.passwordForm.get('oldPassword').value,
        newPassword: this.passwordForm.get('newPassword').value
      }).subscribe(value => {
        if (value.status === 'done') {
          this.successPassword = true;
          this.errorPassword = undefined;
        } else {
          this.successPassword  = false;
          this.errorPassword = value.msg;
        }
      }, error1 => {this.successPassword = false; this.errorPassword = error1; });
    } else {
      this.successPassword = false;
      this.errorPassword = 'كلمه المرور غير صحيحه او غير متطابقه';

    }

  }
  checkPassword(): boolean {
    if (this.passwordForm.get('newPassword').value !== '') {
      return this.passwordForm.get('newPassword').value === this.passwordForm.get('reNewPassword').value;
    }
  }

}
