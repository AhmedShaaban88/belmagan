import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppRoutingRoutingModule} from './app-routing-routing.module';
import { AppComponent } from './app.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {HttpInterceptorService} from './shared/http-interceptor.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AgmCoreModule } from '@agm/core';
import { FooterDetailsComponent } from './footer-details/footer-details.component';
import {ResolveService} from './shared/resolve.service';
import {CommentsService} from './shared/comment_rate.service';
import {NotificationsComponent} from './notifications/notifications.component';
import {StatisticsComponent} from './user-statistics/statistics.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    AccountSettingsComponent,
    HomeComponent,
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
    FooterDetailsComponent,
    NotificationsComponent,
    StatisticsComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    AppRoutingRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBo_V4fZoDdps3HvY-1Kq7Ojl8rBRZTC48'
    }),


  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true}, ResolveService, CommentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
