import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBlogAddComponent } from './user-blog-add.component';

describe('UserBlogAddComponent', () => {
  let component: UserBlogAddComponent;
  let fixture: ComponentFixture<UserBlogAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBlogAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBlogAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
