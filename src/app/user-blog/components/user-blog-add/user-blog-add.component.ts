import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-blog-add',
  templateUrl: './user-blog-add.component.html',
  styleUrls: ['./user-blog-add.component.sass']
})
export class UserBlogAddComponent implements OnInit {
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      desc: ['', [Validators.required, Validators.minLength(8)]],
      category: ['',  Validators.required],
    });
    this.auth.getCateories().subscribe(value => {this.categories = value.data.blogCategories});

  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  newSubmit(): void {
    if (this.newForm.status === 'VALID') {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      form.append('image', this.imageData, this.imageData.name);
      form.append('title', this.newForm.get('name').value);
      form.append('category_id', this.newForm.get('category').value);
      form.append('content', this.newForm.get('desc').value);
      this.auth.addBlog(form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تم بنجاح') {
          this.successNew = true;
          const this_ = this;
          setTimeout(function () {
            this_.router.navigate(['blog/user/view/private/',  value['data'].id]);
          }, 500);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
