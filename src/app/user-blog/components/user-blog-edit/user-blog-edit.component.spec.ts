import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBlogEditComponent } from './user-blog-edit.component';

describe('UserBlogEditComponent', () => {
  let component: UserBlogEditComponent;
  let fixture: ComponentFixture<UserBlogEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBlogEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBlogEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
