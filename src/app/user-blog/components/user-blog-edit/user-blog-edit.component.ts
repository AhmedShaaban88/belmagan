import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-blogs-edit',
  templateUrl: './user-blog-edit.component.html',
  styleUrls: ['./user-blog-edit.component.sass']
})
export class UserBlogEditComponent implements OnInit {
  pageTitle = 'تعديل مدونه';
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];
  currentBlogDetails: any;
  blogID: any;
  blogCategory = undefined;
  changeMainImage = false;
  constructor(private title: Title, private fb: FormBuilder, private auth: AuthService,
              private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => this.categories = value.data.blogCategories, () =>  null,() => {
      this.categories.filter((f, index) => {
        if (this.blogID === f.id.toString()) {
          this.categories.splice(index, 1);
          this.blogCategory = f.title;
        }
      });
    } );
    this.route.data.subscribe((data) => {
      if (data.blog !== undefined) {
        this.blogID = data.blog.data.category_id;
        this.currentBlogDetails = data.blog.data;
        this.newForm = this.fb.group({
          name: [this.currentBlogDetails.title, [Validators.required, Validators.minLength(3)]],
          content: [this.currentBlogDetails.content, [Validators.required, Validators.minLength(8)]],
        });
      }
    });
    this.imageUrl = this.currentBlogDetails.image;
    this.imageData = new File([this.currentBlogDetails.image], this.currentBlogDetails.image.name);
  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
          this.changeMainImage = true;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  newSubmit(): void {
    if (this.newForm.status === 'VALID') {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      if (this.changeMainImage) {
        form.append('image', this.imageData);
      }
      form.append('title', this.newForm.get('name').value);
      form.append('category_id', $('#category option:selected' ).val());
      form.append('content', this.newForm.get('content').value);
      this.auth.updateBlog(this.currentBlogDetails.id, form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تم بنجاح') {
          const x = document.getElementById('toast');
          this.successNew = true;
          x.classList.add('show');
          x.innerHTML = 'تم حفظ التعديل بنجاح';
          setTimeout(function() {
            x.classList.remove('show');
          }, 3000);
          this.router.navigate(['user/blog']);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
