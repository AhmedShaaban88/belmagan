import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-themes',
  templateUrl: './user-blog.component.html',
  styleUrls: ['./user-blog.component.sass'],
})
export class UserBlogComponent implements OnInit {
  pageTitle = 'مدونه جديده';

  constructor(private title: Title, private router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
  }
  openURL(url: string) {
    if (url === '') {
      this.router.navigate(['user/blog']);
    } else {
      this.router.navigate(['user/blog', url]);
    }
  }


}
