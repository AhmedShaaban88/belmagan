import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-blog-active',
  templateUrl: './user-blog-active.component.html',
  styleUrls: ['./user-blog-active.component.sass']
})
export class UserBlogActiveComponent implements OnInit {
  pageTitle = 'المدونات النشطه';
  myBlogActive = [];
  loaderFinished = false;
  errorBlogActive = undefined;
  categories = [];
  articlesActiveCategory = [];
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';


  constructor(private title: Title, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => {this.categories = value.data.blogCategories; });
    this.auth.getMyBlogs().subscribe(value => {this.filterBlog(value.data.blog); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(category: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/blog/edit/${category}/${+id}/${name}`]);
  }
  filterBlog(blogs: any) {
    for (let blog of blogs) {
      if (blog.confirmed === '1') {
        this.myBlogActive.push(blog);
        this.getCatTitleActive(blog.category_id);
      }
    }

    if (this.myBlogActive.length === 0) {
      this.errorBlogActive = 'لا يوجد لديك اي مدونات حتي الان !';
    }
  }
  deleteBlog(blogs: any, id: number | string, currentIndex: number, catArray: any) {
    const conf =  confirm('حذف المدونه ؟');
    if (conf) {
      this.auth.deleteBlog(id).subscribe(value =>  {
        blogs.splice(currentIndex, 1);
        catArray.splice(id, 1);
        if (blogs.length === 0 && blogs === this.myBlogActive) {
          this.errorBlogActive = 'لا يوجد لديك اي مدونات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoBlog(id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`blog/${+id}/${name}/${this.affiliate_code}`]);
  }
  getCatTitleActive(id: number) {
    this.categories.forEach(f => {
      if (f.id === +id) {
        this.articlesActiveCategory.push(f.title);
      }
    });
  }

}
