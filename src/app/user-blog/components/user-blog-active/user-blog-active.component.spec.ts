import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBlogActiveComponent } from './user-blog-active.component';

describe('UserBlogActiveComponent', () => {
  let component: UserBlogActiveComponent;
  let fixture: ComponentFixture<UserBlogActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBlogActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBlogActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
