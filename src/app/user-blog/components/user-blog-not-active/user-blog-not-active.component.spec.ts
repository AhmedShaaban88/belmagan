import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBlogNotActiveComponent } from './user-blog-not-active.component';

describe('UserBlogNotActiveComponent', () => {
  let component: UserBlogNotActiveComponent;
  let fixture: ComponentFixture<UserBlogNotActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBlogNotActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBlogNotActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
