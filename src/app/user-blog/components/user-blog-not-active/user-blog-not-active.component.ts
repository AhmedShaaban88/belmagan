import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-blog-not-active',
  templateUrl: './user-blog-not-active.component.html',
  styleUrls: ['./user-blog-not-active.component.sass']
})
export class UserBlogNotActiveComponent implements OnInit {
  pageTitle = 'المدونات الغير نشطه';
  myBlogNonActive = [];
  loaderFinished = false;
  errorBlogNonActive = undefined;
  categories = [];
  articlesNonActiveCategory = [];
  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => {this.categories = value.data.blogCategories; });
    this.auth.getMyBlogs().subscribe(value => {this.filterBlog(value.data.blog); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(category: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/blog/edit/${category}/${+id}/${name}`]);
  }
  filterBlog(blogs: any) {
    for (let blog of blogs) {
      if (blog.confirmed === '0') {
        this.myBlogNonActive.push(blog);
        this.getCatTitleNonActive(blog.category_id);
      }
    }
    if (this.myBlogNonActive.length === 0) {
      this.errorBlogNonActive = 'لا يوجد لديك اي مدونات حتي الان !';
    }
  }
  deleteBlog(blogs: any, id: number | string, currentIndex: number, catArray: any) {
    const conf =  confirm('حذف المدونه ؟');
    if (conf) {
      this.auth.deleteBlog(id).subscribe(value =>  {
        blogs.splice(currentIndex, 1);
        catArray.splice(id, 1);
        if (blogs.length === 0 && blogs === this.myBlogNonActive) {
          this.errorBlogNonActive = 'لا يوجد لديك اي مدونات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoBlogNonActive(id: string | number) {
    this.loaderFinished = false;
    this.router.navigate([`blog/user/view/private/${+id}`]);
  }
  getCatTitleNonActive(id: number) {
    this.categories.forEach(f => {
      if (f.id === +id) {
        this.articlesNonActiveCategory.push(f.title);
      }
    });
  }

}
