import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';
import {Themes} from '../shared/Themes';

@Injectable()
export class ResolveService implements Resolve<Themes> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Themes> | Observable<any> | Promise<any> {
    const blogID = route.paramMap.get('id');
    const blogName = route.paramMap.get('name');
    const blogcat = route.paramMap.get('category');

    return this.auth.getBlogDetails(blogID).pipe(
     take(1),
     mergeMap(blog => {
       if (blog.status !== 'failed') {
         return of(blog);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
