import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogThemesShellComponent } from './blog-themes-shell.component';

describe('BlogThemesShellComponent', () => {
  let component: BlogThemesShellComponent;
  let fixture: ComponentFixture<BlogThemesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogThemesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogThemesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
