import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserBlogRoutingModule} from './user-blog-routing.module';
import { BlogThemesShellComponent } from './container/blog-shell/blog-themes-shell.component';
import {UserBlogComponent} from './components/user-blog/user-blog.component';
import { UserBlogEditComponent } from './components/user-blog-edit/user-blog-edit.component';
import {AuthService} from './auth.service';
import {ResolveService} from './resolve.service';
import { UserBlogActiveComponent } from './components/user-blog-active/user-blog-active.component';
import { UserBlogNotActiveComponent } from './components/user-blog-not-active/user-blog-not-active.component';
import { UserBlogAddComponent } from './components/user-blog-add/user-blog-add.component';

@NgModule({
  imports: [
    SharedModule,
    UserBlogRoutingModule
  ],
  declarations: [UserBlogComponent, BlogThemesShellComponent, UserBlogEditComponent, UserBlogActiveComponent, UserBlogNotActiveComponent, UserBlogAddComponent],
  providers: [AuthService, ResolveService]
})
export class UserBlogModule { }
