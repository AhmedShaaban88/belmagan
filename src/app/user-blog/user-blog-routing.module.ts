import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BlogThemesShellComponent} from './container/blog-shell/blog-themes-shell.component';
import {UserBlogComponent} from './components/user-blog/user-blog.component';
import {UserBlogEditComponent} from './components/user-blog-edit/user-blog-edit.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {ResolveService} from './resolve.service';
import {UserBlogActiveComponent} from './components/user-blog-active/user-blog-active.component';
import {UserBlogNotActiveComponent} from './components/user-blog-not-active/user-blog-not-active.component';
import {UserBlogAddComponent} from './components/user-blog-add/user-blog-add.component';
const routes: Routes = [
  {
    path: '',
    component: BlogThemesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserBlogComponent,
        children: [
          {
            path: '',
            component: UserBlogAddComponent
          },
          {
            path: 'active',
            component: UserBlogActiveComponent
          },
          {
            path: 'not-active',
            component: UserBlogNotActiveComponent
          }
        ]
      },
      {
        path: 'edit/:category/:id/:name',
        component: UserBlogEditComponent,
        resolve: {
          blog: ResolveService
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserBlogRoutingModule { }
