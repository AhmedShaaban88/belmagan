import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Blog} from '../blog/components/blog-home/blogs';
const headers = new HttpHeaders({
  'Accept': '/',
  'Access-Control-Allow-Methods': '*',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Credentials': 'true'

});
@Injectable()
export class AuthService {
  private apiMyBlogs = 'http://dashboard.belmagan.com/api/MyBlog';
  private addNewBlog = 'http://dashboard.belmagan.com/api/add/blog';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyBlogs(): Observable<any> {
    return this.Http.get<any>(this.apiMyBlogs).pipe(retry(3), catchError(this.handle.handleError));
  }
  deleteBlog(id: number | string) {
    return this.Http.post(`http://dashboard.belmagan.com/api/delete/blog/${+id}`, {}).pipe(retry(3), catchError(this.handle.handleError));
  }
  updateBlog(id: number | string, body: object) {
    return this.Http.post(`http://dashboard.belmagan.com/api/update/blog/${+id}`, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  addBlog(body: object) {
    return this.Http.post(this.addNewBlog, body, {headers: headers}).pipe(retry(0), catchError(this.handle.handleError));

  }
  getCateories(): Observable<any> {
    return this.Http.get<any>('http://dashboard.belmagan.com/api/categories/blog').pipe(retry(3), catchError(this.handle.handleError));
  }
  getBlogDetails(id: number | string): Observable<any> {
    return this.Http.get<Blog>(`http://dashboard.belmagan.com/api/get/blog/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }

}
