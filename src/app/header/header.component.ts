import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { LoginServiceNotUser} from '../shared/login.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import { Meta, Title } from '@angular/platform-browser';
import {AuthService} from '../notifications/auth.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
  providers: [AuthService]
})
export class HeaderComponent implements OnInit {
  user = undefined;
  currentUser = localStorage.getItem('name');
  notificationCount: number;
  constructor( private router: Router, private log: LoginServiceNotUser,
               private http: HttpClient, public meta: Meta, public title: Title,
               private auth: AuthService) { }
  ngOnInit() {

    this.http.get<any>('http://dashboard.belmagan.com/api/setting').subscribe(value => {
      this.meta.updateTag({ name: 'description', content: value.data.desc});
      this.meta.updateTag({ name: 'keywords', content: value.data.tags});
    });
    this.auth.getLastNotifications().subscribe((value: any) => {
      if (value.data) {
        this.notificationCount = value.data.notificationsCount;
      }
    });
    const this_ = this;
    setInterval(function () {
      this_.auth.getLastNotifications().subscribe((value: any) =>{
        if (value.data) {
          this_.notificationCount = value.data.notificationsCount;
        }
      });
    }, 10000);


    $('.hamburger').click (function() {
      $(this).toggleClass('open');
    });
   this.log.loginNotUser().subscribe(value => {
     if (value.status === 'failed') {
       this.user = false;
     } else {
       this.user = true;
     }
   }, error => {
        this.user = false;
      });
}
  logout(): void {
    localStorage.clear();
    this.user = false;
    location.assign('/');
  }
}
