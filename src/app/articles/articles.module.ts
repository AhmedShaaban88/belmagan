import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticleShellComponent } from './container/article-shell/article-shell.component';
import { ArticlesHomeComponent } from './components/articles-home/articles-home.component';
import { ArticleComponent } from './components/article/article.component';
import { ArticleDetailsComponent } from './components/article-details/article-details.component';
import {AuthService} from './auth.service';
import {ResolveServiceArticle, ResolveService, ResolveArticleNotActive} from './resolve.service';
import {DetailsViewComponent} from './components/details-user/details.component';

@NgModule({
  imports: [
    SharedModule,
    ArticlesRoutingModule
  ],
  providers: [AuthService, ResolveService , ResolveServiceArticle, ResolveArticleNotActive],
  declarations: [ArticleShellComponent, ArticlesHomeComponent, ArticleComponent, ArticleDetailsComponent, DetailsViewComponent]
})
export class ArticlesModule { }
