import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleShellComponent } from './article-shell.component';

describe('TrainingShellComponent', () => {
  let component: ArticleShellComponent;
  let fixture: ComponentFixture<ArticleShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
