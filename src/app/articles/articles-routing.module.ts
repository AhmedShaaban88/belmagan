import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArticleShellComponent} from './container/article-shell/article-shell.component';
import {ArticlesHomeComponent} from './components/articles-home/articles-home.component';
import {ArticleComponent} from './components/article/article.component';
import {ArticleDetailsComponent} from './components/article-details/article-details.component';
import {ResolveServiceArticle, ResolveService, ResolveArticleNotActive} from './resolve.service';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {DetailsViewComponent} from './components/details-user/details.component';

const routes: Routes = [
  {
    path: '',
    component: ArticleShellComponent,
    children: [
      {
        path: '',
        component: ArticlesHomeComponent
      },
      {
        path: 'category/:articleID/:articleName',
        component: ArticleDetailsComponent,
        resolve: {
          article: ResolveServiceArticle
        }
      },
      {
        path: ':id/:name/:affiliate_code',
        component: ArticleComponent,
        resolve: {
          article: ResolveService
        }
      },
      {
        path: 'user/view/private/:id',
        component: DetailsViewComponent,
        canActivate: [AuthGaurdService],
        resolve: {
          article: ResolveArticleNotActive
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
