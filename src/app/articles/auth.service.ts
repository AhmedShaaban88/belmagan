import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
   private apiCateories = 'http://dashboard.belmagan.com/api/categories/videos';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getCateories(): Observable<any> {
    return this.Http.get<any>(this.apiCateories).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCategoryData(id: number | string, name: string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/categories/videos/${+id}/${name}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getArticlesDetails(id: number | string, name: string, affiliate_code?: number | string): Observable<any> {
    if (affiliate_code !== 'article' && localStorage.getItem('affiliate_code')) {
      return this.Http.get<any>(`http://dashboard.belmagan.com/api/videos/${+id}/${name}/${affiliate_code}`).pipe(retry(3), catchError(this.handle.handleError));
    }
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/videos/${+id}/${name}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getUserNonActiveArticle(id: number | string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/get/video/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getNextPageCat(id: number | string , name: string, num: number){
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/categories/videos/${+id}/${name}?page=${num}`).pipe(retry(3), catchError(this.handle.handleError));
  }

}
