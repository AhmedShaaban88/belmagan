import {Component, HostListener, OnInit} from '@angular/core';
import {Articles} from '../articles-home/articles';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Themes} from '../../../shared/Themes';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.sass']
})
export class ArticleDetailsComponent implements OnInit {
  articles: Articles;
  pageTitle: string;
  failedMsg: string;
  loaderFinished = false;
  currentPage = 1;
  pageArticle = [];
  pageLoader = false;
  articleName: string;
  articleID: number;
  visitedOnce = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'article';
  constructor(private title: Title, private route: ActivatedRoute, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);

    this.route.data.subscribe((data) => {
      this.pageTitle = data.article.category.title;
      if (data.article.data.blogs !== undefined) {
        this.pageLoader = false;
        this.articles = data.article.data.blogs;
        this.articleID = data.article.category.id;
        this.articleName = data.article.data.blogs[0].slug;
        this.loaderFinished = true;
        this.title.setTitle(` بالمجان - ${this.pageTitle}`);
      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });

  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`articles/${+id}/${title}/${this.affiliate_code}`]);
  }
  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.bottomReached() && !this.pageLoader) {
      this.auth.getNextPageCat(this.articleID , this.articleName , ++this.currentPage).subscribe((value: any) => {
        if (value.data.blogs > 0) {
          this.pageArticle.push(value.data.blogs);
          this.visitedOnce = false;
        } else {
          this.pageLoader = true;
        }
      });
    }
  }
  bottomReached(): boolean {
    if (this.visitedOnce === false && ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200)) {
      this.visitedOnce = true;
      return true;
    }
    return false;
  }
}

