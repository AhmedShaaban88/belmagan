import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AdsService} from '../../../shared/ads.service';

@Component({
  selector: 'app-home',
  templateUrl: './details.component.html',
  styleUrls: ['./detailscomponent.sass'],
})
export class DetailsViewComponent implements OnInit {
  article: any;
  failedMsg: string;
  loaderFinished = false;
  advs: any;
  constructor(private router: Router, private title: Title, private route: ActivatedRoute, private ads: AdsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.article !== undefined && data.article.data !== 'غير موجوده') {
        this.title.setTitle(` بالمجان - ${data.article.data.title}`);
        this.article = data.article.data;
        this.loaderFinished = true;

      } else {
        this.router.navigate(['not-found']);
      }
    }, error => {this.failedMsg = error; this.loaderFinished = false; });
    this.ads.getAds().subscribe(value => this.advs = value['data']);


  }
  openAd(url: string) {
    location.assign(url);
  }




}
