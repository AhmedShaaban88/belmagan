import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../auth.service';
@Component({
  selector: 'app-articles-home',
  templateUrl: './articles-home.component.html',
  styleUrls: ['./articles-home.component.sass']
})
export class ArticlesHomeComponent implements OnInit {
  pageTitle = 'اقسام الشروحات';
  categories: string;
  failedMsg: string;
  loaderFinished = false;
  constructor(private router: Router,  private title: Title, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(categories => this.categories = categories.data.VideoCategories, error => {this.failedMsg = error; this.loaderFinished = true; } , () => this.loaderFinished = true);

  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.router.navigate([`articles/category/${+id}`, title]);
  }


}
