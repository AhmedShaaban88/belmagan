import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Articles} from '../articles-home/articles';
import {CommentsService} from '../../../shared/comment_rate.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as $ from 'jquery';
import {AdsService} from '../../../shared/ads.service';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.sass']
})
export class ArticleComponent implements OnInit {
  article: Articles;
  comments: any;
  user: any;
  failedMsg: string;
  loaderFinished = false;
  userVideos: any;
  relatedVideos: any;
  newForm: FormGroup;
  name = localStorage.getItem('name');
  email = localStorage.getItem('email');
  commentStatus = undefined;
  userRate: any;
  makeArticleRate = false;
  makeUserRate = false;
  theSameUser = false;
  advs: any;
  activeNow: boolean;
  affiliate_code = localStorage.getItem('affiliate_code') || 'article';

  constructor(private title: Title, private route: ActivatedRoute, private router: Router,
              private comment: CommentsService, private fb: FormBuilder, private ads: AdsService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.article.data.video !== undefined) {
        this.article = data.article.data.video;
        this.activeNow = data.article.data.user_is_online;
        this.comments = data.article.data.video.comments;
        this.user = data.article.data.user;
        this.userVideos = data.article.data.uservideos;
        this.relatedVideos = data.article.data.relatedvideos;
        this.loaderFinished = true;
      } else {
        this.loaderFinished = true;
      }
    }, error => {
      this.loaderFinished = false;
    });
    this.title.setTitle(` بالمجان - ${this.article.title}`);
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.email, Validators.required]],
      comment: ['', [Validators.required, Validators.minLength(2)]],
    });
    if (this.user.id.toString() === localStorage.getItem('id')) {
      this.theSameUser = true;
    }
    this.comment.getUserRate(this.user.id).subscribe(value => this.userRate = value.rate);
    this.ads.getAds().subscribe(value => this.advs = value['data']);

  }

  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    window.scrollTo(0, 0);
    this.router.navigate([`articles/${+id}/${title}/${this.affiliate_code}`]);

  }

  addComment(): void {

    this.comment.addComments({
      name: this.newForm.get('name').value || this.name,
      email: this.newForm.get('email').value || this.email,
      comment: this.newForm.get('comment').value,
      type: 'video',
      item_id: this.article.id.toString()
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.commentStatus = true;
        setTimeout(() => {
          this.commentStatus = undefined;
          this.newForm.get('name').patchValue('');
          this.newForm.get('email').patchValue('');
          this.newForm.get('comment').patchValue('');
        }, 2000);
      } else {
        this.commentStatus = false;
      }
    });
  }
  addArticleRate(num: number) {
    this.comment.addRate({
      item_id: this.article.id.toString(),
      type: 'video',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeArticleRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.blog-starts').eq(i).addClass('rate');
    }
  }
  uncolorStars(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.blog-starts').eq(i).css('color', 'grey');
      $('.blog-starts').eq(i).removeClass('rate');
    }

  }
  addUserRate(num: number) {
    this.comment.addRate({
      item_id: this.article.id.toString(),
      type: 'user',
      rate_no: num
    }).subscribe(value => {
      if (value.msg === 'تم بنجاح') {
        this.makeUserRate = true;
        const x = $('#toast');
        x.addClass('show');
        x.text('تم اضافه التقييم بنجاح');
        setTimeout(function() {
          x.removeClass('show');
        }, 3000);
      }
    });
  }
  colorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'rgb(255, 193, 7)');
      $('.user-starts').eq(i).addClass('rate');
    }
  }
  uncolorStarsUser(index: number) {
    for (let i = 4; i >= index; i--) {
      $('.user-starts').eq(i).css('color', 'black');
      $('.user-starts').eq(i).removeClass('rate');
    }

  }
  openAd(url: string) {
    location.assign(url);
  }
  share(site: string) {
    switch (site) {
      case 'fb':
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'twitter':
        window.open(`https://twitter.com/home?status=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'google':
        window.open(`https://plus.google.com/share?url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      case 'pin':
        window.open(`https://pinterest.com/pin/create/button/?url=${window.location.href}&media=${this.article['image']}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
      default:
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${window.location.href}`, '_blank', 'top=100,left=500,width=600,height=500');
        break;
    }
  }

}
