import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';
import {Articles} from './components/articles-home/articles';
import {Themes} from '../shared/Themes';

@Injectable()
export class ResolveService implements Resolve<Articles> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Articles> | Observable<any>| Promise<any> {
    const articleID = route.paramMap.get('id');
    const articleName = route.paramMap.get('name');
    const affiliate_code = route.paramMap.get('affiliate_code') || 'article';
    return this.auth.getArticlesDetails(articleID, articleName, affiliate_code).pipe(
     take(1),
     mergeMap(article => {
       if (article.status !== 'failed') {
         return of(article);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
@Injectable()
export class ResolveServiceArticle implements Resolve<Articles> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Articles> | Observable<any> | Promise<any> {
    const articleID = route.paramMap.get('articleID');
    const articleName = route.paramMap.get('articleName');

    return this.auth.getCategoryData(articleID, articleName).pipe(
      take(1),
      mergeMap(article => {
        if (article.status !== 'failed') {
          return of(article);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}

@Injectable()
export class ResolveArticleNotActive implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Themes> | Observable<any> | Promise<any> {
    const articleID = route.paramMap.get('id');
    return this.auth.getUserNonActiveArticle(+articleID).pipe(
      take(1),
      mergeMap(article => {
        if (article.status !== 'failed') {
          return of(article);
        } else {
          this.router.navigateByUrl('/not-found');
          return EMPTY;
        }
      })
    ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}

