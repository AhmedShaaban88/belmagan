import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  loadedFinished = false;
  onActivate(component: any) {
    if (component) {
      this.loadedFinished = true;
    }
  }

}




