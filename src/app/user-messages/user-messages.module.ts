import {forwardRef, NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserMessagesRoutingModule} from './user-messages-routing.module';
import {UserMessagesComponent} from './components/user-messages/user-messages.component';
import { UserMessagesDetailsComponent } from './components/user-messages-details/user-messages-details.component';
import {ResolveService} from './resolve.service';
import {AuthService} from './auth.service';

@NgModule({
  imports: [
    SharedModule,
    UserMessagesRoutingModule
  ],
  declarations: [UserMessagesComponent, UserMessagesDetailsComponent],
  providers: [AuthService, [forwardRef(() => ResolveService)]]
})
export class UserMessagesModule { }
