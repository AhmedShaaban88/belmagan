import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {

   private allConversationURL = 'http://dashboard.belmagan.com/api/MyConversations';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getAllMessages(): Observable<any> {
     return this.Http.get(this.allConversationURL).pipe(map((value: any) => {
       return value.data;
     }));
  }
  getMessages(id: number| string) {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/show/conversation/${+id}`).pipe(retry(3), catchError(this.handle.handleError));

  }
  sendMessage(id: number | string, message_: string) {
    return this.Http.post(`http://dashboard.belmagan.com/api/send/message/${+id}`, {message: message_});
  }

}
