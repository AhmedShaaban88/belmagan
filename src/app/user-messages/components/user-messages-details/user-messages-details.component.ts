import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-messages-details',
  templateUrl: './user-messages-details.component.html',
  styleUrls: ['./user-messages-details.component.sass'],
})
export class UserMessagesDetailsComponent implements OnInit, AfterViewInit {
  messages = [];
  userInfo: any;
  activeNow: boolean;
  @ViewChild('message') message: ElementRef;
  constructor(private route: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
    this.route.data.subscribe((data) => {
      if (data.messages.data !== undefined) {
        this.activeNow = data.messages.data.is_user_online;
        this.userInfo = data.messages.data.reciever;
        this.messages = data.messages.data.messages;
      } else {
        this.userInfo = {id: data.messages, image: 'https://boards.core77.com/images/avatars/gallery/default-avatar.png', name: 'محادثه جديده'};
      }
    });

    setInterval(() => {
     this.auth.getMessages(this.userInfo.id).subscribe(value => this.messages = value.data.messages);
    }, 4000);

  }
  ngAfterViewInit() {
    document.getElementById('messages').scrollTop =  document.getElementById('messages').scrollHeight;
  }
  sendMessage(message: string) {
    if (!message.trim()) { return; }
    this.auth.sendMessage(this.userInfo.id, message).subscribe(value => {
      this.auth.getMessages(this.userInfo.id).subscribe(value1 => {
        this.messages = value1.data.messages;
        document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight + 100;
      });
    });
    this.message.nativeElement.value = '';

  }


}
