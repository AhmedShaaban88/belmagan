import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMessagesDetailsComponent } from './user-messages-details.component';

describe('UserMessagesDetailsComponent', () => {
  let component: UserMessagesDetailsComponent;
  let fixture: ComponentFixture<UserMessagesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMessagesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMessagesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
