import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';



@Component({
  selector: 'app-user-themes',
  templateUrl: './user-messages.component.html',
  styleUrls: ['./user-messages.component.sass'],
})
export class UserMessagesComponent implements OnInit {
  pageTitle = 'الرسائل';
  allPersons: any;
  error: string;
  loaderFinished = false;

  constructor(private title: Title, private router: Router, private auth: AuthService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getAllMessages().subscribe(value => {
      if (value.length === 0){
        this.error = 'لا يوجد لديك اي رسائل حتي الان';
      } else {
        this.error = undefined;
        this.allPersons = value;

      }
    }, error1 => this.error = 'حدث خطأ اثناء تحميل البيانات', () => this.loaderFinished = true
    );
  }
}
