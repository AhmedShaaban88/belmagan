import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';

@Injectable()
export class ResolveService implements Resolve<any> {
  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const personID = route.paramMap.get('id');

    return this.auth.getMessages(personID).pipe(
     take(1),
     mergeMap(messages => {
       if (messages.status !== 'failed') {
         return of(messages);
       } else {
        return personID;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
