import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserMessagesComponent} from './components/user-messages/user-messages.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {UserMessagesDetailsComponent} from './components/user-messages-details/user-messages-details.component';
import {ResolveService} from './resolve.service';

const routes: Routes = [
  {
    path: '',
    component: UserMessagesComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: 'conversation/:id',
        component: UserMessagesDetailsComponent,
        resolve: {
          messages: ResolveService
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserMessagesRoutingModule { }
