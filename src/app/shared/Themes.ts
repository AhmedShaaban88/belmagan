export interface Themes {
  user: any;
  id: any;
  title: string;
  status: string;
  msg: string;
  data?: string | {
    themes: [{
      id: number,
      title: string,
      description: string,
      content: string,
      download_count: number,
      viewer_count: number,
      cost: string,
      type: string,
      supply: string,
      image: string,
      url: string,
      created_at: {
        date: Date
        timezone_type: number,
        timezone: string
      },
      updated_at: {
        date: Date,
        timezone_type: number,
        timezone: string
      }
    }]
  };
}
export interface IDownload {
  status: string;
  msg: string;
  data?: string;
}

