import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let header;
    const token = localStorage.getItem('token');
    if (token) {
      header = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,

        }
      });} else {
        header = req.clone({
          setHeaders: {
            'Content-Type': 'application/json'
          }
      });
      }
      return next.handle(header);
    }
}
