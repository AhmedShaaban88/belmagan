import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from './handle-http-error.service';

@Injectable({
  providedIn: 'root'
})
export class AdsService {
  private api = 'http://dashboard.belmagan.com/api/ads';
  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  getAds() {
    return this.http.get(this.api).pipe(retry(3), catchError(this.handle.handleError));
  }
}
