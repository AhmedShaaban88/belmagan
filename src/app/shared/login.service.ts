import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  testURL = 'http://dashboard.belmagan.com/api/delete/blog/88888888888888888888888888888888888888888888888';
  value: boolean;
  constructor(private http: HttpClient) { }
  loginUser(): Observable<any>  {
    return this.http.post(this.testURL, {});
  }

}

@Injectable({
  providedIn: 'root'
})
export class LoginServiceNotUser {
  testURL = 'http://dashboard.belmagan.com/api/delete/blog/88888888888888888888888888888888888888888888888';
  constructor(private http: HttpClient) { }
  loginNotUser(): Observable<any>  {
    return this.http.post(this.testURL, {});
  }

}

