import { Injectable } from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HandleHttpErrorService {

  constructor() { }
  handleError (error: HttpErrorResponse): Observable<Error>  {
    if (navigator.onLine === false) {
      return throwError('الانترنت غير موجود');
    } else {
      return throwError('حدث خطأ اثناء تحميل البيانات حاول مره اخري');
    }

  }
}
