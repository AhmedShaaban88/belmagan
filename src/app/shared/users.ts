export interface Users {
  status: string;
  msg: string;
  data: string | {
    token: string;
    user: {
      id: number;
      fname?: string;
      lname?: string;
      name: string;
      email: string;
      phone: number;
      password: number;
      code?: number;
      address?: string;
      bio?: string;
      fb?: string;
      tw?: string;
      web?: string;
      affiliate_code: number;
      created_at?: string;
      updated_at?: string;
    }
  }
}
