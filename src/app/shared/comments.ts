export interface Comments {
  userImage: string;
  userName: string;
  time: Date;
  content: string;
}
