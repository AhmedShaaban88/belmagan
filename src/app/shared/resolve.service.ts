import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {mergeMap, take} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ResolveService implements Resolve<any> {

  constructor(private router: Router, private http: HttpClient) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const footerID = route.paramMap.get('name');
    return this.http.get<any>(`http://dashboard.belmagan.com/api/get/page/${footerID}`).pipe(
     take(1),
     mergeMap(footer => {
       if (footer.status !== 'failed') {
         return of(footer);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
