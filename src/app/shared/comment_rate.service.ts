import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from './handle-http-error.service';

@Injectable()
export class CommentsService {
   private apiComments = 'http://dashboard.belmagan.com/api/comment';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  addComments(body: object): Observable<any> {
    return this.Http.post<any>(this.apiComments, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  getUserRate(id: number | string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/user/rate/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  addRate(body: object): Observable<any> {
    return this.Http.post('http://dashboard.belmagan.com/api/rate', body).pipe(retry(3), catchError(this.handle.handleError));
  }

}
