import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { CKEditorModule } from 'ngx-ckeditor';
@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CKEditorModule
  ],
  declarations: []
})
export class SharedModule { }
