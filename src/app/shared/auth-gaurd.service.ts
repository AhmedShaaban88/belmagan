import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, CanLoad, Route , RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService, LoginServiceNotUser} from './login.service';
@Injectable({
    providedIn: 'root'
})
 export class AuthGaurdService implements CanActivate, CanActivateChild, CanLoad {

   constructor(private log: LoginService) { }
    canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {
      return this.checkLogin();
    }
    canActivateChild(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {
      return this.canActivate(router, state);
    }
    canLoad(route: Route): Observable<boolean> | boolean|  Promise<boolean> {
      return this.checkLogin();
    }
    checkLogin() {
      return this.log.loginUser().toPromise().then(value => {
        if (value.status === 'failed') {
          location.assign('/'); return false;
        } else {
          return true;
        }
      }).catch(e =>  {
        location.assign('/');
        return false;
      });
    }
 }

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdNotUserService implements CanActivate {

  constructor(private log: LoginServiceNotUser) { }
  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean | Promise<boolean> {
  return this.log.loginNotUser().toPromise().then(value => {
    if (value.status === 'failed') {
      return true;
    } else {
    location.assign('/');
    return false;
    }}).catch(e => {
    if (e.statusText === 'Unauthorized') {
      return true;
    }
  });

  }
}
