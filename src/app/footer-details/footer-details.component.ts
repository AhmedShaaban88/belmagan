import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-footer-details',
  templateUrl: './footer-details.component.html',
  styleUrls: ['./footer-details.component.sass'],
})
export class FooterDetailsComponent implements OnInit {
  constructor(private route: ActivatedRoute) { }
  pageDetails: any;
  ngOnInit() {
   this.route.data.subscribe(value => {this.pageDetails = value.footer.data;});
  }

}
