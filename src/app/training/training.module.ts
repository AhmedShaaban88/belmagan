import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import { TrainingRoutingModule } from './training-routing.module';
import {TrainingShellComponent} from './container/training-shell/training-shell.component';
import {TrainingHomeComponent} from './components/training-home/training-home.component';
import { TrainingDetailsComponent } from './components/training-details/training-details.component';
import {AuthService} from './auth.service';
import {ResolveService} from './resolve.service';
import { SafePipe } from './safe.pipe';

@NgModule({
  imports: [
    SharedModule,
    TrainingRoutingModule
  ],
  declarations: [TrainingShellComponent, TrainingHomeComponent, TrainingDetailsComponent, SafePipe],
  providers: [AuthService, ResolveService]
})
export class TrainingModule { }
