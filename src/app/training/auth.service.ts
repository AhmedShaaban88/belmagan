import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private coursesURL = 'http://dashboard.belmagan.com/api/courses';
  constructor(private http: HttpClient, private handle: HandleHttpErrorService) { }
  getAllCourses(): Observable<any> {
    return this.http.get<any>(this.coursesURL).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCourseDetails(id: number | string): Observable<any> {
    return this.http.get<any>(`http://dashboard.belmagan.com/api/course/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  bookCourse(id: number | string) {
    return this.http.post('http://dashboard.belmagan.com/api/book/course', {
      course_id: id
    }).pipe(retry(3), catchError(this.handle.handleError));
  }
  getMyCourses(): Observable<any> {
    return this.http.get<any>('http://dashboard.belmagan.com/api/MyCourses').pipe(retry(3), catchError(this.handle.handleError));
  }

}
