import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-training-details',
  templateUrl: './training-details.component.html',
  styleUrls: ['./training-details.component.sass']
})
export class TrainingDetailsComponent implements OnInit {

  constructor(private title: Title, private route: ActivatedRoute, private course_: AuthService, private router: Router) { }
  course: any;
  loaderFinished = false;
  failedMsg: string;
  main_color: string;
  video_url: string;
  videoDes_url: string;
  book = false;
  bookNow = false;
  ngOnInit() {
    window.scrollTo(0, 0);
    this.route.data.subscribe((data) => {
      if (data.course !== undefined) {
        this.course = data.course.data;
        this.loaderFinished = true;
      } else {
        this.loaderFinished = true;
        this.failedMsg = 'حدثت مشكله بالاتصال بالانترنت';
      }
    }, error => {
      this.failedMsg = error;
      this.loaderFinished = false;
    });
    this.main_color = this.rgb2hex(this.course.color);
    this.video_url = this.emebedVideo(this.course.title_url);
    this.videoDes_url = this.emebedVideo(this.course.learn_url);
    this.title.setTitle(` بالمجان - ${this.course.title}`);
    if (localStorage.getItem('name')) {
    this.course_.getMyCourses().subscribe(value => {
      if (value.data[0].id === this.course.id) {
        this.book = true;
      }
    });
  }
  }
  smoothScroll() {
    $('body, html').animate({
      scrollTop: $('#about').offset().top
    }, 800);
  }
  rgb2hex(hex) {
    let r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16);
      return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + 0.5 + ')';
  }
  emebedVideo(video: string) {
    if (video.indexOf('/watch?v=') !== -1) {
      return video.replace('/watch?v=', '/embed/');
    } else {
      return video;
    }
  }
  bookCourse() {
    if (localStorage.getItem('token')) {
      this.course_.bookCourse(this.course.id).subscribe(value => {
        if (value['status'] !== 'false') {
          this.bookNow = true;
        }
      });
    } else {
      this.router.navigate(['../login']);
    }
  }

}
