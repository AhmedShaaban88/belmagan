import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-articles-home',
  templateUrl: './training-home.component.html',
  styleUrls: ['./training-home.component.sass']
})
export class TrainingHomeComponent implements OnInit {
  parsedArticleTitle: string;
  pageTitle = 'الدورات التعليميه';
  failedMsg: string;
  loaderFinished = false;
  courses: any;
  constructor(private router: Router,  private title: Title, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getAllCourses().subscribe(courses => this.courses = courses.data, error => {this.failedMsg = error; this.loaderFinished = true; }, () =>  this.loaderFinished = true);
  }
  parseNaviagtion(id: number, title: string): void {
    this.loaderFinished = false;
    this.parsedArticleTitle = title.replace(/\s/g, '_').toLowerCase();
  }


}
