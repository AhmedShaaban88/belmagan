import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';

@Injectable()
export class ResolveService implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const courseName = route.paramMap.get('name');
    const courseID = route.paramMap.get('id');


    return this.auth.getCourseDetails(courseID).pipe(
     take(1),
     mergeMap(course => {
       if (course.status !== 'failed') {
         return of(course);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
