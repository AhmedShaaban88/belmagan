import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TrainingShellComponent} from './container/training-shell/training-shell.component';
import {TrainingHomeComponent} from './components/training-home/training-home.component';
import {TrainingDetailsComponent} from './components/training-details/training-details.component';
import {ResolveService} from './resolve.service';

const routes: Routes = [
  {
    path: '',
    component: TrainingShellComponent,
    children: [
      {
        path: '',
        component: TrainingHomeComponent
      },
      {
        path: ':name/:id',
        component: TrainingDetailsComponent,
        resolve: {
          course: ResolveService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingRoutingModule { }
