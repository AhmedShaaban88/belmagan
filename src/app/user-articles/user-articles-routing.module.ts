import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArticlesThemesShellComponent} from './container/articles-shell/articles-themes-shell.component';
import {UserArticlesComponent} from './components/user-articles/user-articles.component';
import {UserArticlesEditComponent} from './components/user-articles-edit/user-articles-edit.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {ResolveService} from './resolve.service';
import {UserArticlesAddComponent} from './components/user-articles-add/user-articles-add.component';
import {UserArticlesActiveComponent} from './components/user-articles-active/user-articles-active.component';
import {UserArticlesNotActiveComponent} from './components/user-articles-not-active/user-articles-not-active.component';
const routes: Routes = [
  {
    path: '',
    component: ArticlesThemesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserArticlesComponent,
        children: [
          {
            path: '',
            component: UserArticlesAddComponent
          },
          {
            path: 'active',
            component: UserArticlesActiveComponent
          },
          {
            path: 'not-active',
            component: UserArticlesNotActiveComponent
          }
        ]
      },
      {
        path: 'edit/:category/:id/:name',
        component: UserArticlesEditComponent,
        resolve: {
          article: ResolveService
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserArticlesRoutingModule { }
