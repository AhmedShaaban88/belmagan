import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesThemesShellComponent } from './articles-themes-shell.component';

describe('BlogThemesShellComponent', () => {
  let component: ArticlesThemesShellComponent;
  let fixture: ComponentFixture<ArticlesThemesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesThemesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesThemesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
