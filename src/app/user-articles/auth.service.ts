import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Articles} from '../articles/components/articles-home/articles';
const headers = new HttpHeaders({
  'Accept': '/',
  'Access-Control-Allow-Methods': '*',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Credentials': 'true'
});
@Injectable()
export class AuthService {
  private apiMyArticles = 'http://dashboard.belmagan.com/api/MyVideos';
  private addNewArticle = 'http://dashboard.belmagan.com/api/add/video';
  private apiCateories = 'http://dashboard.belmagan.com/api/categories/videos';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyArticles(): Observable<any> {
    return this.Http.get<any>(this.apiMyArticles).pipe(retry(3), catchError(this.handle.handleError));
  }
  getCateories(): Observable<any> {
    return this.Http.get<any>(this.apiCateories).pipe(retry(3), catchError(this.handle.handleError));
  }
  deleteArticle(id: number | string) {
    return this.Http.post(`http://dashboard.belmagan.com/api/delete/video/${+id}`, {}).pipe(retry(3), catchError(this.handle.handleError));
  }
  updateArticle(id: number | string, body: object) {
    return this.Http.post(`http://dashboard.belmagan.com/api/update/video/${+id}`, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  addArticles(body: object) {
    return this.Http.post(this.addNewArticle, body, {headers: headers}).pipe(retry(0), catchError(this.handle.handleError));
  }
  getArticlesDetails(id: number | string): Observable<any> {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/get/video/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }

}
