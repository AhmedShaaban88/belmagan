import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';

@Injectable()
export class ResolveService implements Resolve<any> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const articleID = route.paramMap.get('id');
    const articleName = route.paramMap.get('name');
    const articleCat = route.paramMap.get('category');

    return this.auth.getArticlesDetails(articleID).pipe(
     take(1),
     mergeMap(article => {
       if (article.status !== 'failed') {
         return of(article);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
