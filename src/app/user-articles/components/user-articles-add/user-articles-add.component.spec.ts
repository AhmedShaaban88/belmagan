import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArticlesAddComponent } from './user-articles-add.component';

describe('UserArticlesAddComponent', () => {
  let component: UserArticlesAddComponent;
  let fixture: ComponentFixture<UserArticlesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArticlesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArticlesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
