import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-articles-add',
  templateUrl: './user-articles-add.component.html',
  styleUrls: ['./user-articles-add.component.sass']
})
export class UserArticlesAddComponent implements OnInit {
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];
  videosKeys = [];
  videosValues = [];
  constructor( private fb: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.auth.getCateories().subscribe(value => {this.categories = value.data.VideoCategories});
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      desc: ['', [Validators.required, Validators.minLength(8)]],
      videos: this.fb.array([
        this.fb.control('', [Validators.required]),
        this.fb.control('', [Validators.required])
      ]),
      main_photo: ['', Validators.required],
      main_video: ['', Validators.required],
      category: ['',  Validators.required]

    });
  }
  get videos() {
    return this.newForm.get('videos') as FormArray;
  }
  addVideos() {
    this.videos.push(this.fb.control('', Validators.required));
    this.videos.push(this.fb.control('', Validators.required));
  }
  removeVideo(item: any) {
    this.videos.removeAt(item - 1);
    this.videos.removeAt(item);
    this.videos.removeAt(item + 1);
  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  newSubmit(): void {
    this.videosKeys = [];
    this.videosValues = [];
    this.videos.controls.forEach((value, index ) => {if (index % 2 === 0) {
      this.videosKeys.push(value.value);
    } else {
      this.videosValues.push(value.value);
    }
    });

    if (this.newForm.status === 'VALID' && this.imageData) {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      form.append('image', this.imageData, this.imageData.name);
      form.append('title', this.newForm.get('name').value);
      form.append('details', this.newForm.get('desc').value);
      form.append('url', this.newForm.get('main_video').value);
      form.append('category_id', this.newForm.get('category').value);
      this.videosKeys.forEach(f => form.append('titles[]', f));
      this.videosValues.forEach(f => form.append('urls[]', f));
      this.auth.addArticles(form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تمت الإضافة بنجاح') {
          this.successNew = true;
          const this_ = this;
          setTimeout(function () {
            this_.router.navigate(['articles/user/view/private/',  value['data'].id]);
          }, 500);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
