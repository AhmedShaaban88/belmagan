import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-themes',
  templateUrl: './user-articles.component.html',
  styleUrls: ['./user-articles.component.sass'],
})
export class UserArticlesComponent implements OnInit {
  pageTitle = 'مقاله جديده';

  constructor(private title: Title, private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
  }
  openURL(url: string) {
    if (url === '') {
      this.router.navigate(['user/articles']);
    } else {
      this.router.navigate(['user/articles', url]);
    }
  }




}
