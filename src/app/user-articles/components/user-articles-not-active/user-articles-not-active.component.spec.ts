import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArticlesNotActiveComponent } from './user-articles-not-active.component';

describe('UserArticlesNotActiveComponent', () => {
  let component: UserArticlesNotActiveComponent;
  let fixture: ComponentFixture<UserArticlesNotActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArticlesNotActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArticlesNotActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
