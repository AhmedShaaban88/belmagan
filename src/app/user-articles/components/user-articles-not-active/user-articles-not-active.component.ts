import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-articles-not-active',
  templateUrl: './user-articles-not-active.component.html',
  styleUrls: ['./user-articles-not-active.component.sass']
})
export class UserArticlesNotActiveComponent implements OnInit {
  pageTitle = 'المقالات غير النشطه';
  categories = [];
  myArticlesNonActive = [];
  loaderFinished = false;
  errorArticlesNonActive = undefined;
  videosNonActiveCategory = [];
  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => {this.categories = value.data.VideoCategories});
    this.auth.getMyArticles().subscribe(value => {this.filterArticle(value.data.videos); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(category: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/articles/edit/${category}/${+id}/${name}`]);
  }
  filterArticle(articles: any) {
    for (let article of articles) {
      if (article.confirmed === '0') {
        this.myArticlesNonActive.push(article);
        this.getCatTitleNonActive(article.category_id);
      }
    }
    if (this.myArticlesNonActive.length === 0) {
      this.errorArticlesNonActive = 'لا يوجد لديك اي شروحات حتي الان !';
    }
  }
  deleteArticle(articles: any, id: number | string, currentIndex: number, catArray: any) {
    const conf =  confirm('حذف المدونه ؟');
    if (conf) {
      this.auth.deleteArticle(id).subscribe(value =>  {
        articles.splice(currentIndex, 1);
        catArray.splice(id, 1);
        if (articles.length === 0 && articles === this.myArticlesNonActive) {
          this.errorArticlesNonActive = 'لا يوجد لديك اي شروحات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoArticleNonActive(id: string | number) {
    this.loaderFinished = false;
    this.router.navigate([`articles/user/view/private/${+id}`]);
  }
  getCatTitleNonActive(id: number) {
    this.categories.forEach(f => {
      if (f.id === +id) {
        this.videosNonActiveCategory.push(f.title);
      }
    });
  }


}
