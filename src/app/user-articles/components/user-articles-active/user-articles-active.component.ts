import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-articles-active',
  templateUrl: './user-articles-active.component.html',
  styleUrls: ['./user-articles-active.component.sass']
})
export class UserArticlesActiveComponent implements OnInit {
  pageTitle = 'المقالات النشطه';
  loaderFinished = false;
  myArticlesActive = [];
  categories = [];
  errorArticlesActive = undefined;
  videosActiveCategory = [];
  affiliate_codeArticle = localStorage.getItem('affiliate_code') || 'article';

  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => {this.categories = value.data.VideoCategories});
    this.auth.getMyArticles().subscribe(value => {this.filterArticle(value.data.videos); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  gotoEdit(category: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/articles/edit/${category}/${+id}/${name}`]);
  }
  filterArticle(articles: any) {
    for (let article of articles) {
      if (article.confirmed === '1') {
        this.myArticlesActive.push(article);
        this.getCatTitleActive(article.category_id);
      }
    }
    if (this.myArticlesActive.length === 0) {
      this.errorArticlesActive = 'لا يوجد لديك اي شروحات حتي الان !';
    }
  }
  deleteArticle(articles: any, id: number | string, currentIndex: number, catArray: any) {
    const conf =  confirm('حذف المدونه ؟');
    if (conf) {
      this.auth.deleteArticle(id).subscribe(value =>  {
        articles.splice(currentIndex, 1);
        catArray.splice(id, 1);
        if (articles.length === 0 && articles === this.myArticlesActive) {
          this.errorArticlesActive = 'لا يوجد لديك اي شروحات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoArticle(id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`articles/${+id}/${name}/${this.affiliate_codeArticle}`]);
  }
  getCatTitleActive(id: number) {
    this.categories.forEach(f => {
      if (f.id === +id) {
        this.videosActiveCategory.push(f.title);
      }
    });
  }

}
