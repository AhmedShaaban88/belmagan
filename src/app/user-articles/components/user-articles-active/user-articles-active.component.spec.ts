import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArticlesActiveComponent } from './user-articles-active.component';

describe('UserArticlesActiveComponent', () => {
  let component: UserArticlesActiveComponent;
  let fixture: ComponentFixture<UserArticlesActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArticlesActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArticlesActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
