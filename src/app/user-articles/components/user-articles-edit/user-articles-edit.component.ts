import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-articles-edit',
  templateUrl: './user-articles-edit.component.html',
  styleUrls: ['./user-articles-edit.component.sass']
})
export class UserArticlesEditComponent implements OnInit {
  pageTitle = 'تعديل مقاله';
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  videosKeys = [];
  videosValues = [];
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];
  currentArticleDetails: any;
  articleID: any;
  articleCategory = undefined;
  changeMainImage = false;
  constructor(private title: Title, private fb: FormBuilder, private auth: AuthService,
              private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getCateories().subscribe(value => this.categories = value.data.VideoCategories, () =>  null,() => {
      this.categories.filter((f, index) => {
        if (this.articleID === f.id.toString()) {
          this.categories.splice(index, 1);
          this.articleCategory = f.title;
        }
      });
    } );
    this.route.data.subscribe((data) => {
      if (data.article !== undefined) {
        this.articleID = data.article.data.category_id;
        this.currentArticleDetails = data.article.data;
        this.newForm = this.fb.group({
          name: [this.currentArticleDetails.title, [Validators.required, Validators.minLength(3)]],
          content: [this.currentArticleDetails.details, [Validators.required, Validators.minLength(8)]],
          videos: this.fb.array([
            this.fb.control(this.currentArticleDetails.videos[0].title, [Validators.required, Validators.minLength(3)]),
            this.fb.control(this.currentArticleDetails.videos[0].url, [Validators.required, Validators.minLength(3)])
          ]),
          main_video: [this.currentArticleDetails.url, Validators.required],
        });
      }
    });
    this.allVideos();
    this.imageUrl = this.currentArticleDetails.image;
    this.imageData = new File([this.currentArticleDetails.image], this.currentArticleDetails.image.name);
  }
  get videos() {
    return this.newForm.get('videos') as FormArray;
  }
  allVideos() {
    if (this.currentArticleDetails.videos.length > 1) {
      for (let i = 1; i < this.currentArticleDetails.videos.length; i++) {
        this.videos.push(this.fb.control(this.currentArticleDetails.videos[i].title, Validators.required));
        this.videos.push(this.fb.control(this.currentArticleDetails.videos[i].url, Validators.required));
      }
    }
  }
  addVideos() {
    this.videos.push(this.fb.control('', Validators.required));
    this.videos.push(this.fb.control('', Validators.required));
  }
  removeVideo(item: any) {
    this.videos.removeAt(item - 1);
    this.videos.removeAt(item);
    this.videos.removeAt(item + 1);
  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
          this.changeMainImage = true;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  newSubmit(): void {
    this.videosKeys = [];
    this.videosValues = [];
    this.videos.controls.forEach((value, index ) => {if (index % 2 === 0) {
      this.videosKeys.push(value.value);
    } else {
      this.videosValues.push(value.value);
    }
    });
    if (this.newForm.status === 'VALID') {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      if (this.changeMainImage) {
        form.append('image', this.imageData);
      }
      form.append('title', this.newForm.get('name').value);
      form.append('url', this.newForm.get('main_video').value);
      form.append('category_id', $('#category option:selected' ).val());
      form.append('content', this.newForm.get('content').value);
      this.videosKeys.forEach(f => form.append('titles[]', f));
      this.videosValues.forEach(f => form.append('urls[]', f));
      this.auth.updateArticle(this.currentArticleDetails.id, form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تم حفظ التعديل بنجاح') {
          const x = document.getElementById('toast');
          this.successNew = true;
          x.classList.add('show');
          x.innerHTML = 'تم حفظ التعديل بنجاح';
          setTimeout(function() {
            x.classList.remove('show');
          }, 3000);
          this.router.navigate(['user/articles']);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
