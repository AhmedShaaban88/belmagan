import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArticlesEditComponent } from './user-articles-edit.component';

describe('UserBlogEditComponent', () => {
  let component: UserArticlesEditComponent;
  let fixture: ComponentFixture<UserArticlesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArticlesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArticlesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
