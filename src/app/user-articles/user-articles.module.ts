import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserArticlesRoutingModule} from './user-articles-routing.module';

import { ArticlesThemesShellComponent } from './container/articles-shell/articles-themes-shell.component';
import {UserArticlesComponent} from './components/user-articles/user-articles.component';
import { UserArticlesEditComponent } from './components/user-articles-edit/user-articles-edit.component';
import {AuthService} from './auth.service';
import {ResolveService} from './resolve.service';
import { UserArticlesAddComponent } from './components/user-articles-add/user-articles-add.component';
import { UserArticlesActiveComponent } from './components/user-articles-active/user-articles-active.component';
import { UserArticlesNotActiveComponent } from './components/user-articles-not-active/user-articles-not-active.component';

@NgModule({
  imports: [
    SharedModule,
    UserArticlesRoutingModule
  ],
  declarations: [UserArticlesComponent, ArticlesThemesShellComponent, UserArticlesEditComponent, UserArticlesAddComponent, UserArticlesActiveComponent, UserArticlesNotActiveComponent],
  providers: [AuthService, ResolveService]
})
export class UserArticlesModule { }
