import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.sass']
})
export class PageNotFoundComponent implements OnInit {
  pageTitle = 'الصفحه غير موجوده';

  constructor(private title: Title) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

  }

}
