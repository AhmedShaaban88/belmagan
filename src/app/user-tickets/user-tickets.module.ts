import { NgModule } from '@angular/core';
import { UserTicketsRoutingModule } from './user-tickets-routing.module';
import { UserTicketsHomeComponent } from './component/user-tickets-home/user-tickets-home.component';
import { UserTicketsDetailsComponent } from './component/user-tickets-details/user-tickets-details.component';
import { UserTicketsContainerComponent } from './container/user-tickets-container/user-tickets-container.component';
import {AuthService} from './auth.service';
import {SharedModule} from '../shared/shared.module';
import { AllTicketsComponent } from './component/all-tickets/all-tickets.component';
import { UserTicketsCategoryDetailsComponent } from './component/user-tickets-category-details/user-tickets-category-details.component';
import { TicketMessagesComponent } from './component/ticket-messages/ticket-messages.component';

@NgModule({
  imports: [
    SharedModule,
    UserTicketsRoutingModule
  ],
  declarations: [UserTicketsHomeComponent, UserTicketsDetailsComponent, UserTicketsContainerComponent, AllTicketsComponent, UserTicketsCategoryDetailsComponent, TicketMessagesComponent],
  providers: [AuthService]
})
export class UserTicketsModule { }
