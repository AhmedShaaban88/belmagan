import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserTicketsContainerComponent} from './container/user-tickets-container/user-tickets-container.component';
import {UserTicketsHomeComponent} from './component/user-tickets-home/user-tickets-home.component';
import {UserTicketsDetailsComponent} from './component/user-tickets-details/user-tickets-details.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {AllTicketsComponent} from './component/all-tickets/all-tickets.component';
import {UserTicketsCategoryDetailsComponent} from './component/user-tickets-category-details/user-tickets-category-details.component';
import {TicketMessagesComponent} from './component/ticket-messages/ticket-messages.component';

const routes: Routes = [
  {
    path: '',
    component: UserTicketsContainerComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserTicketsHomeComponent
      },
      {
        path: 'new',
        component: UserTicketsDetailsComponent,
        children: [
          {
            path: ':id',
            component: UserTicketsCategoryDetailsComponent

          }
        ]
      },
      {
        path: 'all',
        component: AllTicketsComponent,
      },
      {
        path: 'messages/:id',
        component: TicketMessagesComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserTicketsRoutingModule { }
