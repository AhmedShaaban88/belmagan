import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-all-tickets',
  templateUrl: './all-tickets.component.html',
  styleUrls: ['./all-tickets.component.sass']
})
export class AllTicketsComponent implements OnInit {
  pageTitle = 'كل التذاكر';
  tickets: any;
  loaderFinished = false;
  noTickets: string;
  constructor(private auth: AuthService, private title: Title, private router: Router) { }
  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getMyTickets().subscribe(value =>
    {
      if (value.data.length === 0) {
        this.noTickets = 'لا يوجد لديك اي تذاكر حتي الان ';
      } else {
        this.tickets = value.data;
      }
      this.loaderFinished = true;
    });
  }
  gotoTicket(id: number | string) {
    this.loaderFinished = false;
    this.router.navigate([`user/tickets/messages/${+id}`]);
  }

}
