import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../auth.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-reset-user',
  templateUrl: './user-tickets-details.component.html',
  styleUrls: ['./user-tickets-details.component.sass']
})
export class UserTicketsDetailsComponent implements OnInit {
  pageTitle = 'تذكره جديده';
  categories: any;
  loaderFinished = false;
  constructor(private route: ActivatedRoute, private auth: AuthService, private title: Title) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

    this.auth.getAllCategories().subscribe(value =>
    {
      this.categories = value.data;
      this.loaderFinished = true;
    });
  }

}
