import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTicketsDetailsComponent } from './user-tickets-details.component';

describe('UserTicketsDetailsComponent', () => {
  let component: UserTicketsDetailsComponent;
  let fixture: ComponentFixture<UserTicketsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTicketsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTicketsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
