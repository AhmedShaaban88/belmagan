import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTicketsHomeComponent } from './user-tickets-home.component';

describe('UserTicketsHomeComponent', () => {
  let component: UserTicketsHomeComponent;
  let fixture: ComponentFixture<UserTicketsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTicketsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTicketsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
