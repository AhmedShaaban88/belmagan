import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-reset-home',
  templateUrl: './user-tickets-home.component.html',
  styleUrls: ['./user-tickets-home.component.sass']
})
export class UserTicketsHomeComponent implements OnInit {
  pageTitle = 'التذاكر';
  loaderFininshed = false;
  constructor(private router: Router, private title: Title) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

  }
  openRoute(route: string) {
    this.loaderFininshed = true;
    this.router.navigate(['user/tickets', route]);
  }


}
