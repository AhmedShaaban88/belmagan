import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-ticket-messages',
  templateUrl: './ticket-messages.component.html',
  styleUrls: ['./ticket-messages.component.sass'],
})
export class TicketMessagesComponent implements OnInit {
   ticketMessages: any;
   ticketInfo: any;
  loaderFinished = false;
  message = '';
  pageTitle = 'تذاكري ';

  constructor(private auth: AuthService, private route: ActivatedRoute, private router: Router, private title: Title) { }

  ngOnInit() {
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);

    this.auth.getTicketMessages(this.route.snapshot.paramMap.get('id')).subscribe(value => {
     if (value.data.ticket === null) {
       this.router.navigate(['not-found']);
     } else {
       this.ticketInfo = value.data.ticket;
       this.ticketMessages = value.data.messages;
       this.loaderFinished = true;
     }
      this.loaderFinished = true;

    });
  }
  sendMessage() {
    if (!this.message.trim()) { return; }
    this.auth.sendTicketMessage(this.route.snapshot.paramMap.get('id'), {Message: this.message}).subscribe(value => {
      this.auth.getTicketMessages(this.route.snapshot.paramMap.get('id')).subscribe(value1 => {
        this.ticketInfo = value1.data.ticket;
        this.ticketMessages = value1.data.messages;

      }, () => 0, () => document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight + 500
    );
    });

  }

}
