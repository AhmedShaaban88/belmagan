import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTicketsCategoryDetailsComponent } from './user-tickets-category-details.component';

describe('UserTicketsCategoryDetailsComponent', () => {
  let component: UserTicketsCategoryDetailsComponent;
  let fixture: ComponentFixture<UserTicketsCategoryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTicketsCategoryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTicketsCategoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
