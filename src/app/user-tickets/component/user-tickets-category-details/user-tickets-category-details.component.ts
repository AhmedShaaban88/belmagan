import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-user-tickets-category-details',
  templateUrl: './user-tickets-category-details.component.html',
  styleUrls: ['./user-tickets-category-details.component.sass'],
})
export class UserTicketsCategoryDetailsComponent implements OnInit {
  categoryData: any;
  loaderFininshed = false;
  ticketForm: FormGroup;
  success = undefined;
  formOpened = false;
  errorDate: string;
  constructor(private auth: AuthService, private route: ActivatedRoute, private router: Router, private fb: FormBuilder) {
  }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.auth.getCategoryDetails(this.route.snapshot.paramMap.get('id')).subscribe(value => {
      this.categoryData = value.data;
      this.loaderFininshed = true;
    });
    this.ticketForm = this.fb.group({
      title: ['', [ Validators.required]],
      reason: ['', [Validators.required]]
    });
  }

  ticketSubmit(): void {
      const body = {title: this.ticketForm.get('title').value,
        reason: this.ticketForm.get('reason').value,
        category_id: this.route.snapshot.paramMap.get('id')
      };
      if (this.ticketForm.status === 'VALID') {
        this.auth.createTicket(body).subscribe(value => {
          if (value.status !== 'false') {
            this.errorDate = undefined;
            this.success = true;
            const this_ = this;
            setTimeout(function () {
              this_.router.navigate(['user/tickets/messages', value.data.id])
            }, 500);
          } else {
            this.errorDate = value.data;
            this.success = false;
          }
        }, error => {this.errorDate = error; this.success = false; });
      } else {
        this.errorDate = 'البيانات المدخله غير صحيحه او الخانات الفارغه';
        this.success = false;
      }

    }

}
