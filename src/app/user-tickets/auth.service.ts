import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
 private allCategories = 'http://dashboard.belmagan.com/api/ticket-categories';
 private myTickets = 'http://dashboard.belmagan.com/api/MyTickets';
 private newTikcet = 'http://dashboard.belmagan.com/api/CreateTickets';
  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
 getAllCategories() {
   return this.Http.get<any>(this.allCategories).pipe(retry(3), catchError(this.handle.handleError));
 }
  getCategoryDetails(id: number | string) {
    return this.Http.get<any>(`http://dashboard.belmagan.com/api/ticket-category/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }
  getMyTickets() {
    return this.Http.get<any>(this.myTickets).pipe(retry(3), catchError(this.handle.handleError));
  }
  createTicket(body: object) {
    return this.Http.post<any>(this.newTikcet, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  sendTicketMessage(ticketID: number | string, body: object) {
    return this.Http.post<any>(`http://dashboard.belmagan.com/api/sendTicket/${+ticketID}`, body, {headers: new HttpHeaders('Accept: application/json')}).pipe(retry(3), catchError(this.handle.handleError));
  }
  getTicketMessages(ticketID: number | string) {
    return this.Http.post<any>(`http://dashboard.belmagan.com/api/showTicket/${+ticketID}`, {headers: new HttpHeaders('Accept: application/json')}).pipe(retry(3), catchError(this.handle.handleError));
  }
}
