import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserPurchasesRoutingModule} from './user-purchases-routing.module';

import { PurchasesShellComponent } from './container/purchases-shell/purchases-shell.component';
import {UserPurchasesComponent} from './components/user-purchases/user-purchases.component';

@NgModule({
  imports: [
    SharedModule,
    UserPurchasesRoutingModule
  ],
  declarations: [UserPurchasesComponent, PurchasesShellComponent]
})
export class UserPurchasesModule { }
