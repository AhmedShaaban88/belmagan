import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';

@Injectable()
export class AuthService {
  private apiPurchases = 'http://dashboard.belmagan.com/api/MyPaidThings';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyPurchases(): Observable<any> {
    return this.Http.get<any>(this.apiPurchases).pipe(retry(3), catchError(this.handle.handleError));
  }

}
