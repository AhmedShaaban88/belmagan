import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PurchasesShellComponent} from './container/purchases-shell/purchases-shell.component';
import {UserPurchasesComponent} from './components/user-purchases/user-purchases.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
const routes: Routes = [
  {
    path: '',
    component: PurchasesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserPurchasesComponent
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserPurchasesRoutingModule { }
