import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-themes',
  templateUrl: './user-purchases.component.html',
  styleUrls: ['./user-purchases.component.sass'],
  providers: [AuthService]
})
export class UserPurchasesComponent implements OnInit {
  pageTitle = 'المشتريات';
  activeView = true;
  nonView = false;
  themes: any;
  services: any;
  noThemes: string;
  noServices: string;
  errorThemes = undefined;
  errorServices = undefined;
  loaderThemesFinished = false;
  loaderServiceFinished = false;
  affiliate_code = localStorage.getItem('affiliate_code') || 'details';
  affiliate_codeService = localStorage.getItem('affiliate_code') || 'service';

  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getMyPurchases().subscribe(value => {
      if (value.data.PaidThemes.length === 0) {
        this.noThemes = 'لا يوجد لديك اي قوالب مشتراه حتي الان !';
      } else {
        this.themes = value.data.PaidThemes;
      }
    }, error1 => this.errorThemes = 'حدث خطأ اثناء تحميل البيانات', () => this.loaderThemesFinished = true);
    this.auth.getMyPurchases().subscribe(value => {
      if (value.data.PaidServices.length === 0) {
        this.noServices = 'لا يوجد لديك اي خدمات مشتراه حتي الان !';
      } else {
        this.services = value.data.PaidServices;
      }
    }, error1 => this.errorServices = 'حدث خطأ اثناء تحميل البيانات', () => this.loaderServiceFinished = true);
  }
  download(url: string) {
    location.assign(url);
  }
  openTheme(id: number | string, title: string) {
    this.router.navigate([`themes/paid/${+id}/${title}/${this.affiliate_code}`]);
  }
  openService(id: number | string, title: string) {
    this.router.navigate([`services/${title}/details/${+id}/${this.affiliate_codeService}`]);
  }




}
