import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesShellComponent } from './purchases-shell.component';

describe('BlogThemesShellComponent', () => {
  let component: PurchasesShellComponent;
  let fixture: ComponentFixture<PurchasesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
