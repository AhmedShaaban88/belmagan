import { Injectable } from '@angular/core';
import {Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {mergeMap, take} from 'rxjs/operators';
import {Themes} from '../shared/Themes';

@Injectable()
export class ResolveService implements Resolve<Themes> {

  constructor(private auth: AuthService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<any> | Promise<any> {
    const serviceID = route.paramMap.get('id');
    const serviceName = route.paramMap.get('name');

    return this.auth.getServiceDetails(serviceID).pipe(
     take(1),
     mergeMap(service => {
       if (service.status !== 'failed') {
         return of(service);
       } else {
         this.router.navigateByUrl('/not-found');
         return EMPTY;
       }
    })
   ).toPromise().catch(e => 'حدثت مشكله بالاتصال بالانترنت' );
  }
}
