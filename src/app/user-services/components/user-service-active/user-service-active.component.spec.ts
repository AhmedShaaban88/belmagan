import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserServiceActiveComponent } from './user-service-active.component';

describe('UserServiceActiveComponent', () => {
  let component: UserServiceActiveComponent;
  let fixture: ComponentFixture<UserServiceActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserServiceActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserServiceActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
