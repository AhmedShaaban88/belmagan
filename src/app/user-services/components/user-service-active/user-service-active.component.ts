import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-service-active',
  templateUrl: './user-service-active.component.html',
  styleUrls: ['./user-service-active.component.sass']
})
export class UserServiceActiveComponent implements OnInit {
  pageTitle = 'الخدمات النشطه';
  myServicesActivePaid = [];
  loaderFinished = false;
  errorServicesActive = undefined;
  categories = [];
  affiliate_code = localStorage.getItem('affiliate_code') || 'service';

  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getServicesCat().subscribe(value => {this.categories = value.data});
    this.auth.getMyServices().subscribe(value => {this.filterServices(value.data.services); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  filterServices(services: any) {
    for (let service of services) {
      if (service.confirmed === '1') {
        this.myServicesActivePaid.push(service);
      }
    }
    if (this.myServicesActivePaid.length === 0) {
      this.errorServicesActive = 'لا يوجد لديك اي خدمات حتي الان !';
    }
  }
  deleteTheme(services: any, id: number | string, currentIndex: number) {
    const conf =  confirm('حذف الخدمه ؟');
    if (conf) {
      this.auth.deleteService(id).subscribe(value =>  {
        services.splice(currentIndex, 1);
        if (services.length === 0 && services === this.myServicesActivePaid) {
          this.errorServicesActive = 'لا يوجد لديك اي خدمات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoEdit(type: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/services/edit/${+id}/${name}`]);
  }
  gotoService(id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`services/${name}/details/${+id}/${this.affiliate_code}`]);
  }

}
