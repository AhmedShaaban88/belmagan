import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserServiceNotActiveComponent } from './user-service-not-active.component';

describe('UserServiceNotActiveComponent', () => {
  let component: UserServiceNotActiveComponent;
  let fixture: ComponentFixture<UserServiceNotActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserServiceNotActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserServiceNotActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
