import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-service-not-active',
  templateUrl: './user-service-not-active.component.html',
  styleUrls: ['./user-service-not-active.component.sass']
})
export class UserServiceNotActiveComponent implements OnInit {
  pageTitle = 'الخدمات الغير نشطه';
  myServicesNonActivePaid = [];
  loaderFinished = false;
  errorServicesNonActive = undefined;
  categories = [];
  constructor(private title: Title, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getServicesCat().subscribe(value => {this.categories = value.data});
    this.auth.getMyServices().subscribe(value => {this.filterServices(value.data.services); this.loaderFinished = true; }, () => this.loaderFinished = false);
  }
  filterServices(services: any) {
    for (let service of services) {
      if (service.confirmed === '0') {
        this.myServicesNonActivePaid.push(service);
      }
    }
    if (this.myServicesNonActivePaid.length === 0) {
      this.errorServicesNonActive = 'لا يوجد لديك اي خدمات حتي الان !';
    }
  }
  deleteTheme(services: any, id: number | string, currentIndex: number) {
    const conf =  confirm('حذف الخدمه ؟');
    if (conf) {
      this.auth.deleteService(id).subscribe(value =>  {
        services.splice(currentIndex, 1);
        if (services.length === 0 && services === this.myServicesNonActivePaid) {
          this.errorServicesNonActive = 'لا يوجد لديك اي خدمات حتي الان !';
        }
        const x = document.getElementById('toast');
        x.classList.add('show');
        x.innerHTML = 'تم الحذف بنجاح';
        setTimeout(function() {
          x.classList.remove('show');
        }, 3000);
      });
    }
  }
  gotoEdit(type: string, id: number | string, name: string) {
    this.loaderFinished = false;
    this.router.navigate([`user/services/edit/${+id}/${name}`]);
  }
  gotoServiceNonActive(id: string | number) {
    this.loaderFinished = false;
    this.router.navigate([`services/user/view/private/${+id}`]);
  }

}
