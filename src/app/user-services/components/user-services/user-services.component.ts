import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-services',
  templateUrl: './user-services.component.html',
  styleUrls: ['./user-services.component.sass'],
})
export class UserServicesComponent implements OnInit {
  pageTitle = 'خدمه جديده';
  constructor(private title: Title, private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
  }
  openURL(url: string) {
    if (url === '') {
      this.router.navigate(['user/services']);
    } else {
      this.router.navigate(['user/services', url]);
    }
  }


}
