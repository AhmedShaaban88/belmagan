import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserServiceAddComponent } from './user-service-add.component';

describe('UserServiceAddComponent', () => {
  let component: UserServiceAddComponent;
  let fixture: ComponentFixture<UserServiceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserServiceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserServiceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
