import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-user-service-add',
  templateUrl: './user-service-add.component.html',
  styleUrls: ['./user-service-add.component.sass']
})
export class UserServiceAddComponent implements OnInit {
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  rarFile: any;
  advantagesKeys = [];
  advantagesValues = [];
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.auth.getServicesCat().subscribe(value => {this.categories = value.data});
    this.newForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      category: ['', Validators.required],
      desc: ['', [Validators.required, Validators.minLength(8)]],
      content: ['', [Validators.required, Validators.minLength(8)]],
      features: this.fb.array([
        this.fb.control('', [Validators.required, Validators.minLength(3)]),
        this.fb.control('', [Validators.required, Validators.minLength(3)])

      ]),
      type: ['',  Validators.required],
      cost: ['0', [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]]

    });

  }
  get features() {
    return this.newForm.get('features') as FormArray;
  }
  addFeature() {
    this.features.push(this.fb.control('', Validators.required));
    this.features.push(this.fb.control('', Validators.required));
  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }
  uploadRAR(files: HTMLInputElement) {
    this.rarFile = files.files[0];
  }
  removeFeature(item: any) {
    this.features.removeAt(item - 1);
    this.features.removeAt(item);
    this.features.removeAt(item + 1);

  }

  newSubmit(): void {
    this.advantagesKeys = [];
    this.advantagesValues = [];
    this.features.controls.forEach((value, index ) => {if (index % 2 === 0) {
      this.advantagesKeys.push(value.value);
    } else {
      this.advantagesValues.push(value.value);
    }
    });
    if (this.newForm.get('type').value === 'free') {
      this.newForm.get('cost').patchValue('0');
    }
    if (this.newForm.status === 'VALID' && this.imageData) {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      form.append('image', this.imageData, this.imageData.name);
      form.append('file', this.rarFile, this.rarFile.name);
      form.append('title', this.newForm.get('name').value);
      form.append('description', this.newForm.get('desc').value);
      form.append('type', this.newForm.get('type').value);
      form.append('cost', this.newForm.get('cost').value);
      form.append('category_id', this.newForm.get('category').value);
      form.append('content', this.newForm.get('content').value);
      this.advantagesKeys.forEach(f => form.append('advantages[]', f));
      this.advantagesValues.forEach(f => form.append('values[]', f));
      this.auth.addService(form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تمت الإضافة بنجاح') {
          this.successNew = true;
          const this_ = this;
          setTimeout(function () {
            this_.router.navigate(['services/user/view/private/',  value['data'].id]);
          }, 500);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
