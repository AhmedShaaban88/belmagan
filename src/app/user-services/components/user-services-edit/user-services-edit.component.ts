import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-services-edit',
  templateUrl: './user-services-edit.component.html',
  styleUrls: ['./user-services-edit.component.sass']
})
export class UserServicesEditComponent implements OnInit {
  pageTitle = 'تعديل خدمه';
  newForm: FormGroup;
  imageError: string;
  successNew = undefined;
  imageUrl: any;
  imageData: File;
  rarFile: any;
  advantagesKeys = [];
  advantagesValues = [];
  addError: string;
  submitLoader = true;
  opacity = 1;
  categories = [];
  currentServiceDetails: any;
  hideFileDownload = false;
  serviceID: any;
  serviceCategory = undefined;
  changeMainImage = false;
  changeFile = false;
  constructor(private title: Title, private fb: FormBuilder, private auth: AuthService,
              private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.auth.getServicesCat().subscribe(value => this.categories = value.data, () =>  null, () => {
      this.categories.filter((f, index) => {
        if (this.serviceID === f.id.toString()) {
          this.categories.splice(index, 1);
          this.serviceCategory = f.name;
        }
      });
    } );
    this.route.data.subscribe((data) => {
      if (data.service !== undefined) {
        this.serviceID = data.service.data.sub_service_id;
        this.currentServiceDetails = data.service.data;
        this.currentServiceDetails.type =  this.currentServiceDetails.cost > 0 ? 'paid' : 'free';

        this.newForm = this.fb.group({
          name: [this.currentServiceDetails.title, [Validators.required, Validators.minLength(3)]],
          desc: [this.currentServiceDetails.description, [Validators.required, Validators.minLength(8)]],
          content: [this.currentServiceDetails.content, [Validators.required, Validators.minLength(8)]],
          features: this.fb.array([
            this.fb.control(this.currentServiceDetails.advantages[0].title, [Validators.required, Validators.minLength(3)]),
            this.fb.control(this.currentServiceDetails.advantages[0].value, [Validators.required, Validators.minLength(3)])
          ]),
          type: [this.currentServiceDetails.type,  Validators.required],
          cost: [this.currentServiceDetails.cost, [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]]
        });
      }
    });
    this.allFeatures();
    this.imageUrl = this.currentServiceDetails.image;
    this.imageData = new File([this.currentServiceDetails.image], this.currentServiceDetails.image.name);
    this.rarFile = new File([this.currentServiceDetails.file], this.currentServiceDetails.file.name);
  }
  get features() {
    return this.newForm.get('features') as FormArray;
  }
  allFeatures() {
    if (this.currentServiceDetails.advantages.length > 1) {
      for (let i = 1; i < this.currentServiceDetails.advantages.length; i++) {
        this.features.push(this.fb.control(this.currentServiceDetails.advantages[i].title, Validators.required));
        this.features.push(this.fb.control(this.currentServiceDetails.advantages[i].value, Validators.required));
      }
    }
  }
  addFeature() {
    this.features.push(this.fb.control('', Validators.required));
    this.features.push(this.fb.control('', Validators.required));
  }
  selectImage(input: HTMLInputElement) {
    this.imageData = undefined;
    const file = input.files[0];
    if (file && (file.type.indexOf('image') !== -1)) {
      if (Math.round(file.size / 1024) < 3000) {
        this.imageError = undefined;
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.imageUrl = e.target.result;
          this.changeMainImage = true;
        };
        this.imageData = file;
        reader.readAsDataURL(file);
      } else {
        this.imageError = 'حجم الصوره لايمكن ان يزيد عن 3 ميجا';

      }
    } else {
      this.imageError = 'لابد ان تكون الصوره بصيغه png. او jpeg.';
    }
  }

  uploadRAR(files: HTMLInputElement) {
    this.hideFileDownload = true;
    this.rarFile = files.files[0];
    this.changeFile = true;
  }


  removeFeature(item: any) {
    this.features.removeAt(item - 1);
    this.features.removeAt(item);
    this.features.removeAt(item + 1);

  }

  newSubmit(): void {
    this.advantagesKeys = [];
    this.advantagesValues = [];
    this.features.controls.forEach((value, index ) => {if (index % 2 === 0) {
      this.advantagesKeys.push(value.value);
    } else {
      this.advantagesValues.push(value.value);
    }
    });
    if (this.newForm.get('type').value === 'free') {
      this.newForm.get('cost').patchValue('0');
    }
    if (this.newForm.status === 'VALID') {
      this.opacity = 0.2;
      this.submitLoader = false;
      const form = new FormData();
      if (this.changeMainImage) {
        form.append('image', this.imageData);
      }
      if (this.changeFile) {
        form.append('file', this.rarFile);
      }
      form.append('title', this.newForm.get('name').value);
      form.append('description', this.newForm.get('desc').value);
      form.append('type', this.newForm.get('type').value);
      form.append('cost', this.newForm.get('cost').value);
      form.append('category_id', $('#category option:selected' ).val());
      form.append('content', this.newForm.get('content').value);
      this.advantagesKeys.forEach(f => form.append('advantages[]', f));
      this.advantagesValues.forEach(f => form.append('values[]', f));
      this.auth.updateService(this.currentServiceDetails.id, form).subscribe(value => {
        this.opacity = 1;
        this.submitLoader = true;
        if (value['msg'] === 'تم حفظ التعديل بنجاح') {
          const x = document.getElementById('toast');
          this.successNew = true;
          x.classList.add('show');
          x.innerHTML = 'تم حفظ التعديل بنجاح';
          setTimeout(function() {
            x.classList.remove('show');
          }, 3000);
          this.router.navigate(['user/services']);
        } else {
          this.addError = value['data'];
          this.successNew = false;
        }
      } , error1 =>  {this.addError = error1; this.successNew = false;  this.opacity = 1;
        this.submitLoader = true; });
    } else {
      this.addError = 'البيانات غير صحيحه او غير مكتمله';
      this.successNew = false;
      this.opacity = 1;
      this.submitLoader = true;
    }


  }

}
