import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserServicesEditComponent } from './user-services-edit.component';

describe('UserBlogEditComponent', () => {
  let component: UserServicesEditComponent;
  let fixture: ComponentFixture<UserServicesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserServicesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserServicesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
