import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HandleHttpErrorService} from '../shared/handle-http-error.service';
import {Themes} from '../shared/Themes';
const headers = new HttpHeaders({
  'Accept': '/',
  'Access-Control-Allow-Methods': '*',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Credentials': 'true'

});
@Injectable()
export class AuthService {
  private apiMyServices = 'http://dashboard.belmagan.com/api/MyService';
  private addNewService = 'http://dashboard.belmagan.com/api/add/service';

  constructor(private Http: HttpClient, private handle: HandleHttpErrorService) {}
  getMyServices(): Observable<any> {
    return this.Http.get<any>(this.apiMyServices).pipe(retry(3), catchError(this.handle.handleError));
  }
  deleteService(id: number | string) {
    return this.Http.post(`http://dashboard.belmagan.com/api/delete/service/${+id}`, {}).pipe(retry(3), catchError(this.handle.handleError));
  }
  updateService(id: number | string, body: object) {
    return this.Http.post(`http://dashboard.belmagan.com/api/update/service/${+id}`, body).pipe(retry(3), catchError(this.handle.handleError));
  }
  getServicesCat(): Observable<any> {
    return this.Http.get<any>('http://dashboard.belmagan.com/api/service-sections').pipe(retry(3), catchError(this.handle.handleError));
  }
  addService(body: object) {
    return this.Http.post(this.addNewService, body, {headers: headers}).pipe(retry(0), catchError(this.handle.handleError));

  }
  getServiceDetails(id: number | string): Observable<any> {
    return this.Http.get<Themes>(`http://dashboard.belmagan.com/api/get/service/${+id}`).pipe(retry(3), catchError(this.handle.handleError));
  }

}
