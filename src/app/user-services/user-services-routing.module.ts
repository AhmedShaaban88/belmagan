import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserServicesShellComponent} from './container/user-services-shell/user-services-shell.component';
import {UserServicesComponent} from './components/user-services/user-services.component';
import {UserServicesEditComponent} from './components/user-services-edit/user-services-edit.component';
import {AuthGaurdService} from '../shared/auth-gaurd.service';
import {ResolveService} from './resolve.service';
import {UserServiceAddComponent} from './components/user-service-add/user-service-add.component';
import {UserServiceActiveComponent} from './components/user-service-active/user-service-active.component';
import {UserServiceNotActiveComponent} from './components/user-service-not-active/user-service-not-active.component';
const routes: Routes = [
  {
    path: '',
    component: UserServicesShellComponent,
    canActivateChild: [AuthGaurdService],
    children: [
      {
        path: '',
        component: UserServicesComponent,
        children: [
          {
            path: '',
            component: UserServiceAddComponent
          },
          {
            path: 'active',
            component: UserServiceActiveComponent
          },
          {
            path: 'not-active',
            component: UserServiceNotActiveComponent
          }
        ]
      },
      {
        path: 'edit/:id/:name',
        component: UserServicesEditComponent,
        resolve: {
          service: ResolveService
        }
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserServicesRoutingModule { }
