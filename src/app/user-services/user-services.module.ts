import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {UserServicesRoutingModule} from './user-services-routing.module';

import { UserServicesShellComponent } from './container/user-services-shell/user-services-shell.component';
import {UserServicesComponent} from './components/user-services/user-services.component';
import { UserServicesEditComponent } from './components/user-services-edit/user-services-edit.component';
import {ResolveService} from './resolve.service';
import {AuthService} from './auth.service';
import { UserServiceAddComponent } from './components/user-service-add/user-service-add.component';
import { UserServiceActiveComponent } from './components/user-service-active/user-service-active.component';
import { UserServiceNotActiveComponent } from './components/user-service-not-active/user-service-not-active.component';



@NgModule({
  imports: [
    SharedModule,
    UserServicesRoutingModule,

  ],
  declarations: [UserServicesComponent, UserServicesShellComponent, UserServicesEditComponent, UserServiceAddComponent, UserServiceActiveComponent, UserServiceNotActiveComponent],
  providers: [ResolveService, AuthService]
})
export class UserServicesModule { }
