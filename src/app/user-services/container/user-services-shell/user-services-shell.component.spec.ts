import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserServicesShellComponent } from './user-services-shell.component';

describe('BlogThemesShellComponent', () => {
  let component: UserServicesShellComponent;
  let fixture: ComponentFixture<UserServicesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserServicesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserServicesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
