import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {AuthService} from './auth.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass'],
  providers: [AuthService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  successRegister = undefined;
  pageTitle = 'التسجيل';
  errorData: string | null;
  affiliate_code: any;

  constructor(private fb: FormBuilder, private title: Title, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('affiliate_code') !== 'user') {
      this.affiliate_code = this.route.snapshot.paramMap.get('affiliate_code');
    } else {
      this.affiliate_code = '';
    }
    window.scrollTo(0, 0);
    this.title.setTitle(` بالمجان - ${this.pageTitle}`);
    this.registerForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$')]],
      email: ['', [Validators.email, Validators.required, Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')]],
      phone: ['', [Validators.required, Validators.pattern('^[\+]?[(]?[0-9]{3,}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$')]],
      password: ['',  [Validators.required, Validators.minLength(8),
        Validators.pattern('(?=.*[a-zA-Z\\\u0621-\\\u064A])(?=.*[0-9\\\u0660-\\\u0669])[a-zA-Za-z\\\u0621-\\\u064A0-9\\\u0660-\\\u0669]{8,}$')]],
      conf: ['', [Validators.required, Validators.minLength(8)]],
      code: this.affiliate_code
    });
  }
  registerSubmit(): void {
    if (this.registerForm.status === 'VALID' && this.checkPassword()) {
      this.auth.register(this.registerForm.value).subscribe(value => {
        if (value.status === 'true') {
          this.errorData = null;
          this.successRegister = true;
          localStorage.setItem('token', value.data.token);
          localStorage.setItem('id', value.data.user.id);
          localStorage.setItem('name', value.data.user.name);
          localStorage.setItem('email', value.data.user.email);
          localStorage.setItem('phone', value.data.user.phone);
          localStorage.setItem('affiliate_code', value.data.user.affiliate_code);
         setTimeout(() => {
           location.assign('/');
         }, 500);
        } else {
        this.errorData = value.data;
          this.successRegister = false;
        }
      }, error => {this.errorData = error; this.successRegister = false; });

    } else {
      this.errorData = 'البيانات المدخله غير صحيحه';
      this.successRegister = false;
    }
  }
  checkPassword(): boolean {
    if (this.registerForm.get('password').value !== '') {
      return this.registerForm.get('password').value === this.registerForm.get('conf').value;
    }
  }

}
